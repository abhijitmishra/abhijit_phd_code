import csv,string,re
from sklearn.metrics import classification_report
from skll.metrics import kappa
from numpy import array
def load_resources(pos,neg,pred):
	pos = filterData(list(set(map(lambda x:x.strip().lower(),open(pos,"r").readlines()))))
	neg = filterData(list(set(map(lambda x:x.strip().lower(),open(neg,"r").readlines()))))
	pred = filterData(list(set(map(lambda x:x.strip().lower(),open(pred,"r").readlines()))))
	return [pos,neg,pred]
def filterData(data):
	
	#Remove Unnecessary Noisy portion from the dataset to reduce feature vector size
	#Note: We do not remove stopwords here since stopwords prove to be helpful in SA
	newData = []
	for d in data:
		d=d.lower()
		#remove unprintable chars
		d = filter(lambda x: x in string.printable, d)
		d = d.replace(". "," ")
		d = d.replace("? "," ")
		d = d.replace(", "," ")
		#d = re.sub('[^A-Za-z0-9]+', ' ', d)
		d = d.replace(".","").replace("!","").replace("?","")\
			.replace(",","").replace("#","").replace("$","")\
			.replace("&","").replace("(","").replace(")","")\
			.replace("[","").replace("]","").replace("{","")\
			.replace("}","").replace("-","").replace("@","")\
			.replace("~","").replace(":","").replace("\"","")\
			.replace("'","").replace("%","").replace("__","_").replace("'","").replace("\\","") #Remove multiple occurances of these marks
		
		d = re.sub("\d+[a-z]*","NUM",d)
		#Remove stopwords (as of now, you can switch off if you desperately need them
		#d = " ".join([w if w not in stops else "" for w in d.split()])
		#print d
		newData.append(d)
		
	#This kind of filtering is very important. So make this part more robust as you go on encountering 
	#New cases of noise
	return newData


def removeSubsumed(phraseList):
	removed=[]
	phraseList.sort(key=len)
	for j in range(len(phraseList)-1):
		for ph in phraseList[j+1:]:
			if phraseList[j] in ph:
				removed.append(phraseList[j])
	for h in set(removed):
		phraseList.remove(h)
	return phraseList
	
def predict(inFile,posFile,negFile,predFile):
	lines = filterData(map(lambda x:x.strip().lower(),open(inFile,"r").readlines()))
	posList,negList,predList = load_resources(posFile,negFile,predFile)
	posList = removeSubsumed(posList)
	negList = removeSubsumed(negList)
	predList = removeSubsumed(predList)
	posTotal = list(set(posList+predList))
	gold = [1]*350+[0]*644
	ordered = []
	print len(posTotal)
	#ordered: +ve then ns
	for l in lines:
		pf = [100]
		ns = [100]
		for p in posTotal:			
			pp = l.find(p)
			if pp!=-1:
				pf.append(pp)
		for n in negList:
			nn = l.find(n)
			if nn!=-1:
				ns.append(nn)
		if min(pf)<min(ns):
			ordered.append(1)
		else:
			ordered.append(0)
	unordered = []
	for l in lines:
		pos_pos = 0
		neg_pos = 0
		for p in posTotal:
			pp = l.find(p)
			if pp!=-1:
				pos_pos=pp
				break
		for n in negList:
			nn = l.find(n)
			if nn ==-1:
				neg_pos = nn
				break
		if pos_pos!=0 and neg_pos!=0:
			unordered.append(1)
		else:
			unordered.append(0)
	#print unordered
	prf_ordered = classification_report(array(gold), array(ordered))#average='weighted')
	prf_unordered = classification_report(array(gold), array(unordered))# average='weighted')
	
	print "Ordered: "+str(abs(kappa(array(gold), array(ordered))))
	print "Unordered: "+str(abs(kappa(array(gold), array(unordered))))
	#unordered: both +ve then ns and ns then +ve
	
predict("text.txt","resources/posPhrases.txt","resources/negSituations.txt","resources/posPreds.txt")
