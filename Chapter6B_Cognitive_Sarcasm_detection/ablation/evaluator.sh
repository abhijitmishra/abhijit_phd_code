#! /bin/bash
# Path to weka
WEKA_PATH=$(pwd)
# add mysql-connector (manually copied to weka path) and weka to classpath
CP="$CLASSPATH:/usr/share/java/:$WEKA_PATH/libsvm.jar:$WEKA_PATH/weka.jar"
# use the connector of debian package libmysql-java
# CP="$CLASSPATH:/usr/share/java/:$WEKA_PATH/weka.jar"
echo "used CLASSPATH: $CP"
# start Explorer

training_file=$1

#vals="ngram:1-456 implicit:457-685 largest_series:686 pos:687 neg:688 flip:689 punct:690 lex_pol:691 readability:692 length:693 fixdur:694 fix_count:695 sacc_dist:696 reg_count:697 skip:698 reg2nd1st:699 fix1hwd:700 fix1shwd:701 fix2hwd:702 fix2shwd:703 progcounthwd:704 progcountshwd:705 progdisthwd:706 progdistshwd:707 regcounthwd:708 regcountshwd:709 regdisthwd:710 regdistshwd:711 maxregfrac:712 weightedED:713"
vals="simple:691-699 complex:699-713"
for i in $vals
do
ADD1=`echo $i | cut -d \: -f 1`
ADD2=`echo $i | cut -d \: -f 2`
echo -e "Ablating feature $ADD1"
java -cp $CP -Xmx4g weka.classifiers.meta.FilteredClassifier -v -o -i -t $training_file -F "weka.filters.unsupervised.attribute.Remove -R $ADD2" -W weka.classifiers.functions.LibSVM -- -S 0 -K 0 -D 3 -G 0.0 -R 0.0 -N 0.5 -M 40.0 -C 1.0 -E 0.001 -P 0.1 -Z -seed 1 >"results/$ADD1.log"
done


