#linear mixed effect model
import sys
from pandas import read_csv
import statsmodels.api as sm
import statsmodels.formula.api as smf

fin = "variables.csv"#sys.argv[1]
data = read_csv(fin)
print data
md =smf.mixedlm("Fixation_Duration~Sarcasm", data,groups=data["Doc_ID"])
mdf = md.fit()

print mdf.summary()
