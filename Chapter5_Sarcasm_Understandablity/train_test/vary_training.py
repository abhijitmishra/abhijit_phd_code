import sys

def randomize(l):
	#our pseudo randomization code
	c = []
	rest = []
	flag = 1
	for k in range(len(l)):
		if flag==1:
			c.append(l[k])
			flag=0
			continue
		if flag==0: 
			c.append(l[len(l)-k-1])
			flag=1
			continue
	return c
		
fn = sys.argv[1]
fi = open(fn,"r")
fo1 = open("test_"+fn,"w")
fo2 = open("train_100_"+fn,"w")
fo3 = open("train_90_"+fn,"w")
fo4 = open("train_80_"+fn,"w")
fo5 = open("train_70_"+fn,"w")
fo6 = open("train_60_"+fn,"w")
fo7 = open("train_40_"+fn,"w")
f_meta = open("meta_"+fn,"w")

heads = []
data_flag = 0
pos_data = {}
neg_data = {}
sid = 0
for l in fi:
	l = l.strip()
	if l=="":
		continue
	if l.__contains__("@data"):
		heads.append("@data\n")
		data_flag = 1
	if data_flag ==0:
		heads.append(l)
	else:
		l = l.strip()
		inf_l = l.split(",")
		sid+=1
		if inf_l[-1] =="1":
			pos_data[sid] = l
		elif inf_l[-1] =="0":
			neg_data[sid] = l
		elif "{" in inf_l[-1]:
			if inf_l[-2] =="1":
				pos_data[sid] = l
			elif inf_l[-2] =="0":
				neg_data[sid] = l
		else:
			print "Some junk lines."
pos_ids = pos_data.keys()
neg_ids = neg_data.keys()

test_pos = []
train_100_pos = [] 
test_neg = []
train_100_neg = []

#criteria_pos = int(len(pos_ids)*0.2)
#criteria_neg = int(len(neg_ids)*0.2)
#print criteria_neg


i =0
for ii in pos_ids:
	if ((i+1)%5)==0:
		test_pos.append(pos_data[ii])
		f_meta.write(str(i+1)+"\n")
	else:
		train_100_pos.append(pos_data[ii])
	i+=1
i=0
for ii in neg_ids:
	if ((i+1)%5)==0:
		test_neg.append(neg_data[ii])
		f_meta.write(str(i+1)+"\n")
	else:
		train_100_neg.append(neg_data[ii])
	i+=1
	
train_90_neg = randomize(randomize(train_100_neg[:int(len(train_100_neg)*0.9)]))
train_80_neg = randomize(randomize(train_100_neg[:int(len(train_100_neg)*0.8)]))
train_70_neg = randomize(randomize(train_100_neg[:int(len(train_100_neg)*0.7)]))
train_60_neg = randomize(randomize(train_100_neg[:int(len(train_100_neg)*0.6)]))
train_40_neg = randomize(randomize(train_100_neg[:int(len(train_100_neg)*0.4)]))
train_90_pos = randomize(randomize(train_100_pos[:int(len(train_100_pos)*0.9)]))
train_80_pos = randomize(randomize(train_100_pos[:int(len(train_100_pos)*0.8)]))
train_70_pos = randomize(randomize(train_100_pos[:int(len(train_100_pos)*0.7)]))
train_60_pos = randomize(randomize(train_100_pos[:int(len(train_100_pos)*0.6)]))
train_40_pos = randomize(randomize(train_100_pos[:int(len(train_100_pos)*0.4)]))

print len(train_100_neg)+len(train_100_pos)
print len(train_90_neg)+len(train_90_pos)
print len(train_80_neg)+len(train_80_pos)
print len(train_70_neg)+len(train_70_pos)
print len(train_60_neg)+len(train_60_pos)
print len(train_40_neg)+len(train_40_pos)
for h in heads:
	fo1.write(h+"\n")
	fo2.write(h+"\n")
	fo3.write(h+"\n")
	fo4.write(h+"\n")
	fo5.write(h+"\n")
	fo6.write(h+"\n")
	fo7.write(h+"\n")
for t in test_pos:
	fo1.write(t+"\n")
for t in test_neg:
	fo1.write(t+"\n")
for t in train_100_pos:
	fo2.write(t+"\n")
for t in train_100_neg:
	fo2.write(t+"\n")
for t in train_90_pos:
	fo3.write(t+"\n")
for t in train_90_neg:
	fo3.write(t+"\n")
for t in train_80_pos:
	fo4.write(t+"\n")
for t in train_80_neg:
	fo4.write(t+"\n")
for t in train_70_pos:
	fo5.write(t+"\n")
for t in train_70_neg:
	fo5.write(t+"\n")
for t in train_60_pos:
	fo6.write(t+"\n")
for t in train_60_neg:
	fo6.write(t+"\n")
for t in train_40_pos:
	fo7.write(t+"\n")
for t in train_40_neg:
	fo6.write(t+"\n")
