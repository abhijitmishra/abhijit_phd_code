#arff_merger

import sys

file1 = sys.argv[1]
file2 = sys.argv[2]
out = open(sys.argv[3],"w")

attribs1 = []
attribs2 = []

data_list1 = []
data_list2 = []
with open(file1,"r") as f1:
	for l in f1:
		l = l.strip()
		if l.__contains__("@relation"):
			continue
		if l.__contains__("attribute") and not(l.__contains__("Label")):
			attribs1.append(l)
		else:
			inf = l.split(",")
			if inf[-1]=="1" or inf[-1]=="-1":
				data_list1.append(",".join(inf[:-1]))
with open(file2,"r") as f2:
	for l in f2:
		l = l.strip()
		if l.__contains__("@relation"):
			continue
		if l.__contains__("attribute") and not(l.__contains__("Label")):
			attribs2.append(l)
		else:
			inf = l.split(",")
			if inf[-1]=="1" or inf[-1]=="-1":
				data_list2.append(",".join(inf))
				
out.write("@relation sarcasm\n\n")
for attrib in attribs1:
	out.write(attrib+"\n")
for attrib in attribs2:
	out.write(attrib+"\n")
#out.write("@attribute Label {1,0}")
out.write("\n\n@data\n")

for i in range(len(data_list1)):
	out.write(data_list1[i]+","+data_list2[i]+"\n")
out.close()
