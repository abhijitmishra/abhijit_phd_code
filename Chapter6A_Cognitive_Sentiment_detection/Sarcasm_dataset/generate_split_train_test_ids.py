# This programme extracts a "fair" training-holdout split
#We have 994 instances: 383 Positive, 611 negative (350 sarcastic, 261 non-sarcastic)
#Out of this we have to select 33 positive samples, 114 sarcastic-negative and 114 non-sarcastic negative
#22 out of the sarcastic things are positive (banters)- so excluded. Rest positive 361

sarcastic_ids = []
positive_ids = []	
non_sarcastic_negative_ids = []
positive_heldout_count = 43
sarcastic_negative_heldout_count = 114
non_sarcastic_negative_heldout_count = 114

import sys
import random
fn = open(sys.argv[1],"r")
out = open("INFO_"+sys.argv[1],"w") 
flag=0
idt = 1
for line in fn:
	if line.__contains__("@data"):
		#ignore till you encounter @data in the arff file
		flag=1
	elif flag==1:
		line = line.strip()
		if line=="":
			continue
		line_inf = line.split(",")
		label = line_inf[-1]	
		if label.strip() == "1":
			positive_ids.append(idt)
		elif idt<351:
			sarcastic_ids.append(idt)
		elif label.strip() == "-1":
			non_sarcastic_negative_ids.append(idt)
		idt+=1
print str(len(sarcastic_ids))+":"+str(len(positive_ids))+":"+str(len(non_sarcastic_negative_ids))
sarcastic_test = random.sample(sarcastic_ids,131)
positive_test = random.sample(positive_ids,33)
negative_test = random.sample(non_sarcastic_negative_ids,130)

sarcastic_test.sort()
positive_test.sort()
negative_test.sort()

out.write("Sarcastic Test: "+str(sarcastic_test)+"\n")
out.write("Positive Test: "+str(positive_test)+"\n")
out.write("Negative Test: "+str(negative_test)+"\n")
out.write("Sarcastic Negative: "+str(sarcastic_ids)+"\n")
out.write("Positive: "+str(positive_ids)+"\n")
out.write("Negative: "+str(non_sarcastic_negative_ids)+"\n")
