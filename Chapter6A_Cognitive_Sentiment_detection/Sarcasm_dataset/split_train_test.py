# This programme extracts a "fair" training-holdout split
#We have 994 instances: 383 Positive, 611 negative (350 sarcastic, 261 non-sarcastic)
#Out of this we have to select 33 positive samples, 114 sarcastic-negative and 114 non-sarcastic negative

import sys
import random

fn = open(sys.argv[1],"r")
sarcastic_test = [2, 4, 6, 8, 11, 14, 19, 20, 22, 23, 25, 32, 37, 41, 46, 47, 49, 54, 57, 58, 59, 63, 66, 70, 73, 76, 78, 82, 90, 92, 93, 94, 96, 97, 102, 103, 105, 110, 114, 115, 117, 119, 121, 122, 123, 127, 131, 132, 134, 135, 136, 137, 138, 143, 147, 148, 152, 153, 155, 156, 161, 163, 164, 168, 170, 172, 174, 175, 176, 177, 178, 186, 188, 191, 192, 194, 203, 205, 210, 212, 214, 218, 219, 221, 222, 227, 228, 231, 235, 236, 240, 241, 242, 243, 247, 248, 250, 251, 255, 256, 268, 269, 270, 271, 273, 278, 280, 282, 285, 289, 294, 295, 297, 298, 299, 313, 318, 319, 320, 322, 325, 326, 327, 331, 334, 337, 340, 343, 344, 348, 349]
positive_test = [64, 100, 149, 352, 378, 413, 418, 420, 425, 507, 509, 533, 535, 565, 573, 577, 585, 590, 618, 624, 685, 715, 726, 738, 750, 794, 861, 876, 881, 894, 899, 938, 940]
negative_test = [353, 360, 365, 377, 392, 414, 417, 430, 435, 442, 443, 450, 451, 455, 457, 458, 459, 460, 461, 463, 465, 468, 472, 473, 475, 477, 483, 493, 495, 496, 497, 498, 504, 505, 506, 510, 512, 516, 542, 545, 551, 562, 564, 582, 589, 593, 597, 605, 608, 614, 617, 627, 629, 638, 642, 647, 650, 654, 658, 662, 664, 666, 670, 671, 673, 679, 681, 687, 694, 696, 699, 707, 709, 712, 716, 719, 722, 730, 732, 734, 744, 745, 757, 762, 763, 766, 775, 783, 786, 789, 790, 793, 796, 799, 800, 815, 817, 821, 834, 836, 837, 840, 842, 847, 868, 869, 870, 886, 890, 892, 893, 895, 903, 904, 906, 916, 917, 922, 930, 934, 942, 981, 983, 984, 985, 987, 989, 990, 992, 994]

print str(len(sarcastic_test))+":"+str(len(positive_test))+":"+str(len(negative_test))
out1 = open("train_test/train_"+sys.argv[1],"w")
out2 = open("train_test/test_"+sys.argv[1],"w")
flag=0
idt = 1
for line in fn:
	if line.__contains__("@data"):
		#ignore till you encounter @data in the arff file
		flag=1
		out1.write(line)
		out2.write(line)
	elif flag==1:
		line = line.strip()
		if line=="":
			continue
		if (idt in sarcastic_test) :
			out2.write(line+"\n")
		elif (idt in positive_test):
			out2.write(line+"\n")
		elif (idt in negative_test):
			out2.write(line+"\n")
		else:
			out1.write(line+"\n")
		idt+=1
	else:
		#write headers
		out1.write(line)
		out2.write(line)

