#Load sentiment lexicon
from sklearn.metrics import precision_recall_fscore_support,roc_auc_score,confusion_matrix
def evaluate(prediction, actual):
    precision, recall, f_beta,support = precision_recall_fscore_support(actual,prediction,average=None)
    auc = roc_auc_score(actual,prediction)
    return [precision,recall,f_beta,auc]
def calculateAccuracy(prediction, actual):
    prediction = list(prediction)
    correct_labels = filter(lambda x:prediction[x]==actual[x],range(len(prediction)))
    accuracy = float(len(correct_labels))/float(len(prediction))
    return accuracy
def classwiseAccuracy(prediction,actual):
    posAccuracy = 0
    negAccuracy = 0
    posCount = 0
    negCount = 0
    prediction = list(prediction)
    i = 0
    for i in range(len(prediction)):
        if actual[i]==1:
            if prediction[i]==1:
                posAccuracy +=1
            posCount+=1
        elif actual[i]==-1:
            if prediction[i]==-1:
                negAccuracy +=1
            negCount+=1
    return [float(posAccuracy)/float(posCount),float(negAccuracy)/float(negCount)]
def loadLexicon(lexicon_path):
	f = open(lexicon_path,"r")
	lex = {}
	for line in f:
		word, val = line.strip().split()
		lex[word.lower()] = int(val)
	return lex

def getSentimentOfSentence(sentence,lex,negators,intensifiers):
	#apply filters here
	sentence = sentence.replace(",","")
	pos_score = 0
	neg_score = 0
	word_count = 0
	multiplier = 1
	words = sentence.split()
	pos_words = ""
	neg_words = ""
	for word in words:
		curr_score = lex.get(word, 0)
		if (multiplier != 1 and curr_score != 0):
			curr_score = curr_score * multiplier
			multiplier = 1
		if word in intensifiers:
			multiplier = 2
		if word in negators:
			multiplier *= -1
		if (curr_score < 0):
			neg_words+=word+" "
			neg_score += curr_score
		elif (curr_score > 0):
			pos_words+=word+" "
			pos_score += curr_score	
	if (pos_score != 0 or neg_score != 0):		
		if (pos_score > abs(neg_score)):
			#return pos_score
			return 1
		elif (abs(neg_score) >pos_score):
			#return neg_score
			return -1
	return 0

negators = ["not","no","never","neither","nor"]
intensifiers = ["absolute","abundant","acute","ample","all-consuming","all-embracing","ardent","big","bottomless","boundless","burning","categorical","certain","clear","close","colossal","complete","consuming","consummate","considerable","damned","decided","deep","definite","definitive","downright","drastic","emphatic","enormous","endless","entire","excessive","extensive","extravagant","extreme","fanatical","fervent","fervid","fierce","firm","forceful","gigantic","great","greatest","grievous","heightened","high","highest","huge","humongous","illimitable","immense","incalculable","incontestable","incontrovertible","indisputable","infinite","inordinate","intense","intensified","intensive","keen","mammoth","marked","maximal","maximum","mighty","more","most","mungo","numerous","out-and-out","outright","perfect","plain","powerful","prodigious","profound","pronounced","pure","real","resounding","severe","sharp","sheer","simple","strict","strong","stupendous","supreme","sure","terrible","thorough","titanic","top","total","towering","tremendous","true","ultimate","unambiguous","unconditional","uncontestable","undeniable","undesputable","unending","unequivocal","unfathomable","unlimited","unmistakable","unqualified","unquestionable","utmost","utter","uttermost","vast","vehement","vigorous","violent","vivid","zealous","absolutely","absurdly","abundantly","acutely","all","altogether","amazingly","amply","ardently","astonishingly","awfully","categorically","certainly","clearly","completely","considerably","dearly","decidedly","deeply","definitely","definitively","downright","drastically","eminently","emphatically","endlessly","entirely","even","exaggeratedly","exceedingly","excessively","explicitly","expressly","extensively","extraordinarily","extravagantly","extremely","fanatically","fervently","fervidly","fiercely","firmly","forcefully","frankly","fully","greatly","highly","hugely","immensely","incredibly","indeed","indispensably","indisuptably","indubitably","infinitely","inordinately","intensely","irretrievably","just","keenly","largely","maximally","mightily","more","most","much","notably","noticeably","outright","outrightly","particularly","perfectly","plainly","positively","powerfully","pressingly","pretty","prodigiously","profoundly","purely","quite","really","remarkably","severely","sharply","simply","strikingly","strongly","stupendously","substantially","super","superlatively","supremely","surely","surpassingly","surprisingly","terribly","thoroughly","too","totally","tremendously","truely","ultimately","unambiguously","uncommonly","unconditionally","unbelievably","undeniably","undisputably","unequivocally","unnaturally","unquestionably","unusually","utterly","vastly","vehemently","vigorously","violently","vividly","very","wholly","wonderfully","zealously"]

if __name__=="__main__":
	import sys
	input_file = open(sys.argv[1],"r")
	lex = loadLexicon("sentiwordlist")
	gold = []
	prediction = []
	for line in input_file:
		line  = line.strip()
		if line.__contains__("@attribute") or line.__contains__("@relation") or line.__contains__("@data") or line=="":
			continue
		gp = line.split(",")[-1]
		if gp.strip()=="pos":
			gp = 1
		elif gp.strip()=="neg":
			gp = -1
		else:
			continue
		gold.append(gp)
		line = line.replace(",pos","").replace(",neg","")
		polarity = getSentimentOfSentence(line,lex,negators,intensifiers)
		prediction.append(polarity)
	print "Test instances: "+str(len(gold))	
	print "Test Accuracy: "+str(calculateAccuracy(prediction,gold))
	print "Classwise Accuracy: "+str(classwiseAccuracy(prediction,gold))
	print "Precision, Recall,F, AUC" +str(evaluate(prediction,gold))
	print"Predictions: "
	for k in prediction:
		print k
