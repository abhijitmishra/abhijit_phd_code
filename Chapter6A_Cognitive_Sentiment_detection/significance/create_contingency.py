#create contigency table from weka output dump
import sys

fn1  = sys.argv[1]
fn2 = sys.argv[2]

predicted_1 = []
predicted_2 = []
with open (fn1) as f1:
	for line in f1:
		flag = 0
		for i in range(len(line)):
			if line[i]==":":
				if flag==0:
					flag=1
				else:
					predicted_1.append(line[i-1])
		
with open (fn2) as f2:
	for line in f2:
		flag = 0
		for i in range(len(line)):
			if line[i]==":":
				if flag==0:
					flag=1
				else:
					predicted_2.append(line[i-1])

pos_pos = 0
pos_neg = 0
neg_pos = 0
neg_neg = 0
total=0	
for i in range(len(predicted_1)):
	total+=1
	p1 = predicted_1[i]
	p2 = predicted_2[i]
	if p1=="1" and p2=="1":
		pos_pos+=1
	elif p1=="1" and p2=="2":
		pos_neg+=1
	elif p1=="2" and p2=="1":
		neg_pos+=1
	elif p1=="2" and p2=="2":
		neg_neg+=1
	else:
		print "Something wrong in "+str(i)
				
print "Pos:Pos "+str(pos_pos)
print "Pos:Neg "+str(pos_neg)
print "Neg:Pos "+str(neg_pos)
print "Neg:Neg "+str(neg_neg)
print "Total: "+ str(total) 
