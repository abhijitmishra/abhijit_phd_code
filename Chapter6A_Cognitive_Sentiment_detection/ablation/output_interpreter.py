#Interpret output
import sys,os


#take input weka log_files

def get_stats(infps):
	precision_pos = []
	precision_neg = []
	precision_avg = []

	recall_pos = []
	recall_neg = []
	recall_avg = []

	f_pos = []
	f_neg = []
	f_avg = []

	kappa = []
	for fps in infps:
		with open(fps) as f:
			lines = f.readlines()
			for i in range(len(lines)):
				line = lines[i]
				line = line.strip()
				if "Kappa statistic" in line:
					line_inf = line.split()
					kappa.append(float(line_inf[2]))
				elif "TP Rate" in line:
					neg = lines[i+2].strip()
					pos = lines[i+1].strip()
					avg = lines[i+3].strip().replace("Weighted Avg.","")
					pos = pos.split()
					neg = neg.split()
					avg = avg.split()
					
					precision_pos.append(float(pos[2]))
					recall_pos.append(float(pos[3]))
					f_pos.append(float(pos[4]))
					
					precision_neg.append(float(neg[2]))
					recall_neg.append(float(neg[3]))
					f_neg.append(float(neg[4]))
					
					precision_avg.append(float(avg[2]))
					recall_avg.append(float(avg[3]))
					f_avg.append(float(avg[4]))
	return [precision_pos,recall_pos,f_pos,precision_neg,recall_neg,f_neg,precision_avg,recall_avg,f_avg]

d= sys.argv[2]
stats = {}
folds = map(str,range(1,11))
import os

for g in os.listdir(d):
	if "log" in g:
		infps = d+"/"+g
		print infps
		g = g.replace(".log","")
		stats[g] = get_stats([infps])
		
with open(sys.argv[1],"w") as f1:
	f1.write("Config,Precision_pos,Precision_neg,Precision_avg,Recall_pos,Recall_neg,Recall_avg,F-Score_pos,F-Score_avg,F-Score_avg\n")
	for c in stats.keys():
		precision_pos,recall_pos,f_pos,precision_neg,recall_neg,f_neg,precision_avg,recall_avg,f_avg = stats[c]
		pp = sum(precision_pos)/len(precision_pos)
		rp = sum(recall_pos)/len(recall_pos)
		fp = sum(f_pos)/len(f_pos)
		
		pn = sum(precision_neg)/len(precision_neg)
		rn = sum(recall_neg)/len(recall_neg)
		fn = sum(f_neg)/len(f_neg)
		
		pa = sum(precision_avg)/len(precision_avg)	
		ra = sum(recall_avg)/len(recall_avg)
		fa = sum(f_avg)/len(f_avg)
		f1.write(c+","+str(pp)+","+str(pn)+","+str(pa)+","+str(rp)+","+str(rn)+","+str(ra)+","+str(fp)+","+str(fn)+","+str(fa)+"\n")
