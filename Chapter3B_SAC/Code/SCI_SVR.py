'''
Created on 10-Nov-2013

@author: abhijit
'''
import codecs
from numpy import min,max
from numpy import array
import svmlight
import sys
import pickle
import math
from random import shuffle
from scipy import stats
from scipy.stats.stats import pearsonr


def getNormalizeSingle(imin,imax,minMax,val):
	#For single valued. This is used from testing one sentence at a time.
	dmin = minMax[0]
	dmax = minMax[1]
	normalized = imin + (imax - imin)*(val - dmin)/(dmax - dmin)
	return normalized

def getNormalizedValues(imin,imax,minMax,arr):
	#Normalize the whole array
	"""I = Imin + (Imax-Imin)*(D-Dmin)/(Dmax-Dmin)"""
	dmin = minMax[0]
	dmax = minMax[1]
	normalized = imin + (imax - imin)*(arr - dmin)/(dmax - dmin)
	return normalized

def writeMinMaxValues(f,fPath):
	with open(fPath, "w") as fl:
		pickle.dump(f, fl)
def readMinMaxValues(fPath): 
	with open(fPath, "r") as fl:
		return pickle.load(fl)

def normalizeData(filePath,flag):
	dataDict = {}
	with codecs.open(filePath,"r","utf-8") as f:
		reader = map(lambda z:z.split("\t"),map(lambda x:x.strip("\n"),f.readlines()))
		trans = zip(*reader)
		for t in trans:
			dataDict[t[0].strip()] = t[1:]
		dataDict.pop("Sentence")
		keys = dataDict.keys()
		keys.sort()
		
		if flag =="training":
			minMax = {}
			for k in keys:
				arr = array(map(lambda x:float(x),dataDict[k]))
				min1 = min(arr)
				max1 = max(arr)
				minMax[k] = [min1,max1]
			writeMinMaxValues(minMax,"data/minMax.dat")
		normDataDict = {}
		minMax = readMinMaxValues("data/minMax.dat")
		normFeatures=[]
		normLabels=[]
		for k in keys:
			if k!="SCI_label":
				normDataDict[k] = getNormalizedValues(0.1,1,minMax[k],array(map(lambda x:float(x),dataDict[k])))
			#normFeatures.append(normDataDict[k])
			else:
				normLabels=getNormalizedValues(1,10,minMax[k],array(map(lambda x:float(x),dataDict[k])))

		normFeatures = map(lambda x:normDataDict[x] if x!="SCI_label" else None,normDataDict)
		i =0
		finalData = []
		for l in normLabels:
			featureIndex = 1
			featureList = []
			for feature in normFeatures:
				if(feature!=None):
					featureList.append((featureIndex,feature[i]))
					featureIndex+=1
			t = (l,featureList)
			finalData.append(t)		 
			i+=1
	#calculate baseline accuracy before returning the values. A bit of a mess.
	return [normLabels,finalData]

'''
def loadData(filePath,flag):
	with codecs.open(filePath,"r","utf-8") as f:
		data = map(lambda x:x.strip("\n"),f.readlines()) #excluding header
		header = data[1:]
		data = data[1:]
		tuples = map(lambda x:x.split("::"),data)
		sSet = zip(*tuples)  #Transpose
		fSet = sSet[2:] #Excluding sentences All features present)
		lSet = sSet[1]
		print "....................."
		if (flag == "training"):
			f = {}
		#print lSet
		normLabels = getNormalizedValues(0,1,array(map(lambda x:float(x),lSet)))
		normFeatures = map(lambda elemt:getNormalizedValues(0,1,array(map(lambda x:float(x),elemt))),fSet)
		i =0
		finalData = []
		for l in normLabels:
			featureIndex = 1
			featureList = []
			for feature in normFeatures:
				featureList.append((featureIndex,feature[i]))
				featureIndex+=1
			t = (l,featureList)
			finalData.append(t)
			
			i+=1
	return [normLabels,finalData]
'''
def train(trainingData):
	#model = svmlight.learn(trainingData, type='regression',C=20,kernel='polynomial',poly_degree=3,verbosity=2)
	model = svmlight.learn(trainingData, type='regression',C=0.8)
	svmlight.write_model(model, 'data/SCI_Model.dat')
def test(testData):
	model = svmlight.read_model('data/SCI_Model.dat')
	predictions = svmlight.classify(model, testData)
	return predictions
def baseline(orig,baseLineData):
	return None
	#take sentence length as a measurement of complexity
	#take 
def MSE(orig,pred):
	#Mean Squared error computation
	orig = map(lambda x:list(x)[0],orig)
	correl = pearsonr(orig,pred)
	binAccuracy = sum(map(lambda x:0 if math.fabs(orig[x]-pred[x])>1 else 1,range(len(orig)))) 
	binAccuracy =float(binAccuracy)/len(orig) 
	meAbsolute = sum(map(lambda x:math.fabs(orig[x]-pred[x]),range(len(orig)))) 
	mePercentage = sum(map(lambda x:(math.fabs(orig[x]-pred[x])/orig[x])*100/len(orig),range(len(orig)))) 
	meAbsolute /= len(orig)
	msePercentage = sum(map(lambda x:math.pow((orig[x]-pred[x])/orig[x],2),range(len(orig))))
	mse = sum(map(lambda x:math.pow((orig[x]-pred[x]),2),range(len(orig))))
	mse = float(mse)/len(orig)
	msePercentage/=len(orig)*100	
	return [mse,meAbsolute,mePercentage,msePercentage,correl,binAccuracy]

def k_fold_cross_validation(items, k, randomize=False):
	fold = []
	total_mse  = 0
	total_me_percentage = 0
	total_me_absolute = 0
	total_mse_percentage = 0
	total_correlation = 0
	total_bin_accuracy = 0
	if randomize:
		items = list(items)
		shuffle(items)

	slices = [items[i::k] for i in xrange(k)]

	for i in xrange(k):
		validation = slices[i]
		training = [item
					for s in slices if s is not validation
					for item in s]
		fold.append([training, validation])
	fold_id=1
	for f in fold:
		train(f[0])
		pred = test(f[1])
		mse,meAbsolute,mePercentage,msePercentage,correl,binAccuracy = MSE(f[1],pred)
		print >>sys.stderr, "Mean Square Error in Fold "+str(fold_id)+":-> " + str(mse)
		print >>sys.stderr, "Mean Absolute Error in Fold "+str(fold_id)+":-> " + str(meAbsolute)
		print >>sys.stderr,"Mean Percentage Error in fold: "+str(fold_id)+":-> "+str(mePercentage)
		print >>sys.stderr,"Mean Square Percentage Error in fold: "+str(fold_id)+":-> "+str(msePercentage)
		print >>sys.stderr,"Correlation with the observed data"+str(fold_id)+":-> "+str(correl[0])
		total_mse+=mse
		total_me_percentage+=mePercentage
		total_me_absolute+=meAbsolute
		total_mse_percentage+=msePercentage
		total_correlation += correl[0]
		total_bin_accuracy+=binAccuracy
		fold_id+=1
	print >>sys.stderr, str(float(total_mse)/k)
	print >>sys.stderr, str(float(total_me_absolute)/k)
	print >>sys.stderr, str(float(total_me_percentage)/k)	
	print >>sys.stderr, str(float(total_correlation)/k)
	print >>sys.stderr, str(float(total_bin_accuracy)/k)
	#print >>sys.stderr, "Total mse: "+ str(float(total_mse)/k)
	#print >>sys.stderr, "Total mean absolute: "+ str(float(total_me_absolute)/k)
	#print >>sys.stderr, "Total mean percentage error: "+ str(float(total_me_percentage)/k)
	#print >>sys.stderr, "Total mean square percentage error: "+ str(float(total_mse_percentage)/k)
		
		

def main():
	trData = sys.argv[1]
	#tsData = sys.argv[2]
	k = sys.argv[2]
	data =normalizeData(trData,"training")[1]
	print len(data)
	k_fold_cross_validation(data,int(k))
	#train(data)
	#testData = normalizeData(tsData,"test")[1]
	#shuffle(testData)
	#testData = testData[200:]
	#pred = test(testData)
	#mse,meAbsolute,mePercentage,msePercentage,correl,binAccuracy = MSE(testData,pred)
	#print mse
	#print meAbsolute
	#print mePercentage
	#print correl
if __name__ == "__main__":
	main()
