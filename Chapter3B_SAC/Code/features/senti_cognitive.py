#find Subjective word count + position of 1st positive and negative words
import codecs, os, string, sys
from pascual_tools import *
from sentiwordnet import SentiWordNetCorpusReader
from nltk import word_tokenize
from nltk import pos_tag
from nltk.corpus import wordnet as wn
  
base_path = os.path.dirname(os.path.abspath(__file__))
SWN_FILENAME = base_path+"/../resources/SentiWordNet_3.0.0_20130122.txt"
if os.path.exists(SWN_FILENAME):
  swn = SentiWordNetCorpusReader(SWN_FILENAME)

def get_senti_details(sentence):
  tokens = word_tokenize(sentence)
  pos_tags = pos_tag(tokens)
  subjective_count=0
  subjective_score = 0
  positive_flag = -1
  negative_flag = -1
  tag =""
  word_index = 0
  for t in pos_tags:
    #print t	
    if t[1][0]=='N':
      tag = 'n'
    elif t[1][0]=='V':
      tag ='v'
    elif t[1][0]=='R':   
      tag = 'r'
    elif t[1][0]=='A':
      tag = 'a'
    if (tag!=""):
      #print tag
      lemma = wn.morphy(t[0])
      if lemma is None:
        lemma=t[0]
	
      if swn.senti_synsets(lemma,tag):
        sentival = swn.senti_synsets(lemma,tag)[0] #first sense
        subjective_count+=1
        pos = sentival.pos_score
        neg = sentival.neg_score
        if pos<neg:
          subjective_score+=neg
          if negative_flag==-1:
            negative_flag=word_index
        elif pos>neg:
          subjective_score+=pos
          if positive_flag==-1:
            positive_flag=word_index
        word_index+=1
  return[subjective_count,subjective_score,positive_flag,negative_flag]

print get_senti_details("brady achieves the remarkable feat of squandering a topnotch foursome of actors . . . by shoving them into every cliched white-trash situation imaginable .")
'''
with open(sys.argv[1],"r") as f:
  dat = map(lambda x:x.strip("\n"),f.readlines())
  output = open("output.feat","w")
  for s in dat:
    subjective_count,subjective_score,positive_flag,negative_flag = get_senti_details(s)
    output.write(str(subjective_count)+"\t"+str(subjective_score)+"\t"+str(positive_flag)+"\t"+str(negative_flag)+"\n")
  output.close()
'''
