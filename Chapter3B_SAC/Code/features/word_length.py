#!/usr/bin/python

import codecs, os, sys
from nltk.corpus import cmudict
from pascual_tools import *
d = cmudict.dict()

'''
Compute the number of syllables for a certain word,
using the CMU pronunciation dictionary.
'''
def nsyl(word):
  if word.lower() in d.keys():
    return average([len(list(y for y in x if y[-1].isdigit()))\
      for x in d[word.lower()]])
  else:
    return 0

def compute_syllable_count(sentence):
  syllables_per_word = []
  syllables_per_word.extend(\
    map(lambda x: nsyl(x), sentence.strip().split()))
  syl_count = average(syllables_per_word)
  return syl_count

def main(args = None):
  import textwrap
  interactive = False
	
  if len(sys.argv) > 1:
    if sys.argv[1] == "--interactive":
      interactive = True
  output = open("syllable_count", "w")
  while True:
    if interactive:
      sentence = raw_input("Please enter a sentence: ")
    else:
      sentence = raw_input()

    if sentence == "":
      USAGE=textwrap.dedent("""\
      Usage:
          word_length.py <tokenized filename>
      """)
      break
  
    syllables_per_word = []
    syllables_per_word.extend(\
      map(lambda x: nsyl(x), sentence.strip().split()))
    if(interactive):
      print "Average syllable count is: "+str(average(syllables_per_word))
    else:
      output.write(str(average(syllables_per_word)) + '\n')
  if (not(interactive)):
    output.close()
if __name__ == '__main__':
  main()
