# This program gives the percentage of verbs in a sentence

import sys
from corenlp import *
from corenlpConfig import corenlp_dir
interactive = False

if len(sys.argv) > 1:
	if sys.argv[1] == "--interactive":
		interactive = True
		print "Loading components..."

corenlp = StanfordCoreNLP(corenlp_dir)
output = open("percentage-of-verbs", "w")

while True:
	if interactive:
		sentence = raw_input("Please enter a sentence: ")
	else:
		sentence = raw_input()

	if sentence == "":
		break

	parseoutput = eval(corenlp.parse(sentence));
	words = parseoutput["sentences"][0]["words"]

	verbs = 0

	for word in words:
		pos = word[1]["PartOfSpeech"]
		if (pos[0] == 'V'): verbs +=1

	total = len(words)

	pn = float(verbs) / total

	if interactive:
		print "The percentage of verbs in the sentence is: ", pn
	else:
		output.write(str(pn) + '\n')

output.close()
