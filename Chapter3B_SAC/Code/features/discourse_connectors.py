#!/usr/bin/python

import codecs, os, string, sys
from pascual_tools import *
from nltk.corpus import wordnet as wn

base_path = os.path.dirname(os.path.abspath(__file__))
"""
Load the dictionary containing the words in the Discourse Connecors (AWL).
"""
finput_dc = codecs.open(base_path+'/../resources/discourse_connectors.txt', 'r', 'utf-8')
dc_word_lines = finput_dc.readlines()
dc_phrases = map(lambda x: x.strip(), dc_word_lines)

"""
Check if a word (or its lemma) are not punctuations and whether they are in
the Academic Word List (AWl).
"""
def dc_finder(text,lst):
  return map(lambda x:1 if text.__contains__(x) else 0,lst)
def count_discourse_connectors(sentence):
  return sum(dc_finder(sentence,dc_phrases))
def main(args = None):
  import textwrap
  interactive = False
	
  if len(sys.argv) > 1:
    if sys.argv[1] == "--interactive":
      interactive = True
  output = open("discourse_connectors", "w")
  while True:
    if interactive:
      sentence = raw_input("Please enter a sentence: ")
    else:
      sentence = raw_input()

    if sentence == "":
      USAGE=textwrap.dedent("""\
      Usage:
          discourse_connectors.py <sentence-splitted, tokenized and lowercased filename>
      """)
      break
    sentence = sentence.decode("utf-8")
    discourse_connectors = dc_finder(sentence,dc_phrases)
    if(interactive):
      print "Average number of discourse connectors is: "+str(sum(discourse_connectors))
    else:
      output.write(str(sum(discourse_connectors)) + '\n')
  if (not(interactive)):
    output.close()

if __name__ == '__main__':
  main()
