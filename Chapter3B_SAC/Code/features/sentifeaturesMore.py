#!/usr/bin/python

import codecs, os, string, sys
from pascual_tools import *
from nltk.corpus import wordnet as wn

base_path = os.path.dirname(os.path.abspath(__file__))
"""
Load the dictionary containing the words in the General Listing Service (GSL).
"""
finput_senti = open(base_path+'/../resources/sentiwordlist', 'r')
dictionary = {}
lines = finput_senti.readlines()
for l in lines:
	inf = l.split()
	if(len(inf)==2):
		dictionary[inf[0]]=inf[1]
"""
Check if a word (or its lemma) are not punctuations and whether they are in
the General Listing Service or Academic Word List.
"""
def getSentiProperties(sentence):
  negCount = 0
  posCount = 0
  words = sentence.split()
  prevPolarity = 0
  sequence = ""
  flips = 0
  notFlag = 0
  for w in words:
    lemma = wn.morphy(w)
    if lemma is None:
      lemma = w
    sentiVal = dictionary.get(lemma,"-3")
    if sentiVal=="-3":
      print lemma
    elif sentiVal=="0":
      
      if (notFlag==1):
        sequence+="+"
        posCount+=1
        if(prevPolarity==-1):
          flips+=1		
        prevPolarity = 1
      else:
        sequence+="-"
        negCount+=1
        if(prevPolarity==1):
          flips+=1
        prevPolarity = -1

    elif sentiVal == "+1":
      if (notFlag==1):
        sequence+="-"
        negCount+=1
        if(prevPolarity==1):
          flips+=1		
        prevPolarity = -1
      else:
        sequence+="+"
        posCount+=1
        if(prevPolarity==-1):
          flips+=1
        prevPolarity = 1
        
    if (w.lower()=="not"):
      notFlag=1
    else:
      notFlag=0
  subjCount=posCount+negCount
  return [subjCount,posCount,negCount,flips,sequence]
  
def main(args = None):
  import textwrap
  interactive = False
	
  if len(sys.argv) > 1:
    if sys.argv[1] == "--interactive":
      interactive = True
    
  output = open("sentiProp.csv", "w")
  while True:
    if interactive:
      sentence = raw_input("Please enter a sentence: ")
    else:
      sentence = raw_input()
    sentence = sentence.replace("n't","not")
    subjCount,posCount,negCount,flips,sequence = getSentiProperties(sentence)
    if(interactive):
      print "Senti properties: "+str([subjCount,posCount,negCount,flips,sequence])
    else:
      output.write(str(subjCount)+"\t"+str(posCount)+"\t"+str(negCount)+"\t"+str(flips)+"\t"+str(sequence)+ '\n')
  if (not(interactive)):
    output.close()
if __name__ == '__main__':
  main()
