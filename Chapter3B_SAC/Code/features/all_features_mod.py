'''
Created on 19-Nov-2013
Computes all the features and returns a dictionary
@author: abhijit
'''

from coreference_distance import compute_coref_distance
from degree_of_polysemy import dp
from structural_complexity import compute_sc
from word_length import compute_syllable_count
from discourse_connectors import count_discourse_connectors
from terminal_nonterminal import terminal_non_terminal_ratio
from percentage_noun_verb_prep import count_prepositions_noun_verb
from out_vocabulary import out_vocabulary
from numbers import num_count
from named_entity import named_entities
from length import length
from height_hypernym import height_hypernym_sentence
from passive import compute_passive
from corenlp import StanfordCoreNLP
from corenlpConfig import corenlp_dir
from sentiwords import get_pos_neg_score
import sys
#Maintain sequence of the functions
#L~DP~SC~Ddensity~CorrDist~DisCon~HeighHyp~NE~NumCount~OOV~Passive~Pprep~Pnoun~Pverb ~~SyllCount

corenlp = StanfordCoreNLP(corenlp_dir)
def all_features(text):
    
    parseoutput = eval(corenlp.parse(text))
    feat_values = {}
    print >> sys.stderr, "............Linguistic Features........"
    print >> sys.stderr ,  "Computing Length..."
    feat_values[u'L'] = length(text)
    print >> sys.stderr ,  "Computing Degree of Polysemy..."
    #print >> sys.stderr ,  "flag"
    feat_values[u'DP'] = dp(text)
    #print >> sys.stderr ,  "flag"
    print >> sys.stderr ,  "Computing Structural Complexity (dependency distance)..."
    feat_values[u'SC'] =compute_sc(text.replace("-","@"),parseoutput) #This replacement is to avoid confusion
    #print >> sys.stderr ,  "flag"
    print >> sys.stderr ,  "Computing Structural Complexity (Non terminal to terminal ratio)..."
    feat_values[u'Ddensity'] = terminal_non_terminal_ratio(text,parseoutput)
    #print >> sys.stderr ,  "flag"
    print >> sys.stderr ,  "Computing corefference distance.."
    feat_values[u'CorrDist'] = compute_coref_distance(text.decode("utf-8"),parseoutput)
    #print >> sys.stderr ,  "flag"
    print >> sys.stderr ,  "Computing discourse connector count..."
    feat_values[u'DisCon'] = count_discourse_connectors(text)
    #print >> sys.stderr ,  "flag"
    print >> sys.stderr ,  "Computing eight of hypernymy..."
    #print >> sys.stderr ,  "flag"
    print >> sys.stderr ,  "Computing Named Entity Count..."
    #print >> sys.stderr ,  "flag"
    print >> sys.stderr ,  "Computing digit count..."
    #print >> sys.stderr ,  "flag"
    print >> sys.stderr ,  "Computing average OOV words based on GSL and AWL lists..."
    #print >> sys.stderr ,  "flag"
    #print >> sys.stderr ,  "flag"   
    print >> sys.stderr ,  "Computing average number of syllables... "
    #print >> sys.stderr ,  "flag"
    print >> sys.stderr ,  "Computing percentage of noun verb and prepositions..."
    feat_values[u'Pprep'],feat_values[u'Pnoun'],feat_values[u'Pverb'] = count_prepositions_noun_verb(text,parseoutput)
    print >>sys.stderr, ".....Sentiment Features..........."
    feat_values['PosWords'],feat_values['NegWords'],feat_values['PosScore'],feat_values['NegScore'],feat_values['SentiSeq']= get_pos_neg_score(text)

    return feat_values
#test
from nltk.tokenize.punkt import PunktWordTokenizer
import codecs

if __name__ == '__main__':
    if (len(sys.argv)==1):
        print "Please input a sentence"
        sentence = raw_input()
        print all_features(sentence)
        exit(0)
    with codecs.open(sys.argv[1],"r","utf-8") as f:
        lines = map(lambda x:x.strip("\n"),f.readlines())
        w =""
        keyPrint=1
        for sentence in lines:
            sentence = " ".join(PunktWordTokenizer.tokenize(PunktWordTokenizer(),sentence))
            f= all_features(sentence,sys.argv[2])
            keys = f.keys()
            keys.sort()
            if (keyPrint==1):
                w+=",".join(keys)+"\n"
                keyPrint=0
            for k in keys:
                w+=str(f[k])+","
            w = w.strip(",")+"\n"
        w.strip("\n")
        with codecs.open(sys.argv[1]+".feat","w","utf-8") as f2:
            f2.write(w)
