# This program gives the length of a sentence

import sys

def length(sentence):
	return len(sentence.split())
def main():
	interactive = False
	
	if len(sys.argv) > 1:
		if sys.argv[1] == "--interactive":
			interactive = True
	output = open("length","w")
	while True:
		if interactive:
			sentence = raw_input("Please enter a sentence: ")
		else:
			sentence = raw_input()
	
		if sentence == "":
			break
	
		words = sentence.split()
	
		if interactive:
			print "The length of the sentence is: ", len(words)
		else:
			output.write(str(len(words))+"\n")
	if not(interactive):
		output.close()
if __name__ == '__main__':
  main()
