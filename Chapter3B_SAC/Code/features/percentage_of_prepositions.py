# This program gives the percentage of prepositions in a sentence

import sys
from corenlp import *
from corenlpConfig import corenlp_dir
interactive = False

def count_prepositions(sentences):
	return None
if len(sys.argv) > 1:
	if sys.argv[1] == "--interactive":
		interactive = True
		print "Loading components..."

corenlp = StanfordCoreNLP(corenlp_dir)
output = open("percentage-of-prepositions", "w")

while True:
	if interactive:
		sentence = raw_input("Please enter a sentence: ")
	else:
		sentence = raw_input()

	if sentence == "":
		break

	parseoutput = eval(corenlp.parse(sentence));
	words = parseoutput["sentences"][0]["words"]

	prepositions = 0

	for word in words:
		pos = word[1]["PartOfSpeech"]
		if ((pos == 'IN') or (pos == 'TO')): prepositions +=1

	total = len(words)

	pn = float(prepositions) / total

	if interactive:
		print "The percentage of prepositions in the sentence is: ", pn
	else:
		output.write(str(pn) + '\n')

output.close()
