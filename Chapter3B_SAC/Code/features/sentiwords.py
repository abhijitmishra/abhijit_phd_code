#!/usr/bin/python

import codecs, os, string, sys
from pascual_tools import *
from sentiwordnet import SentiWordNetCorpusReader
from nltk import word_tokenize
from nltk import pos_tag
from nltk.corpus import wordnet as wn
  
base_path = os.path.dirname(os.path.abspath(__file__))
SWN_FILENAME = base_path+"/../resources/SentiWordNet_3.0.0_20130122.txt"
if os.path.exists(SWN_FILENAME):
  swn = SentiWordNetCorpusReader(SWN_FILENAME)
def get_pos_neg_score(sentence):
  tokens = word_tokenize(sentence)

  pos_tags = pos_tag(tokens)
  pos_score = 0
  neg_score = 0
  pos_wc = 0
  neg_wc = 0
  tag = ""
  sequence = ""
  for t in pos_tags:
    #print t	
    if t[1][0]=='N':
      tag = 'n'
    elif t[1][0]=='V':
      tag ='v'
    elif t[1][0]=='R':   
      tag = 'r'
    elif t[1][0]=='A':
      tag = 'a'
    if (tag!=""):
      print tag
      lemma  = wn.morphy(t[0])
      senti_syn = swn.senti_synsets(t[0])
      if (senti_syn!=[]):
        first_senti_synset = swn.senti_synsets(t[0])[0]
        pos_score += first_senti_synset.pos_score
        neg_score += first_senti_synset.neg_score
        if (first_senti_synset.pos_score>first_senti_synset.neg_score):
	  #print "Pos:"+t[0]
          pos_wc+=1
	  sequence+="+ "
	  print t[0]
	if (first_senti_synset.pos_score<first_senti_synset.neg_score):
	  #print "Neg:"+t[0]
	  neg_wc+=1
	  sequence+="- "
  return [pos_score,neg_score,pos_wc,neg_wc,sequence]

def main(args = None):
  import textwrap
  interactive = False
	
  if len(sys.argv) > 1:
    if sys.argv[1] == "--interactive":
      interactive = True
    
  output = open("out_of_vocabulary", "w")
  while True:
    if interactive:
      sentence = raw_input("Please enter a sentence: ")
    else:
      sentence = raw_input()
    sentence = sentence.decode("utf-8").lower()
    if sentence == "":
      USAGE=textwrap.dedent("""\
      Usage:
          sentiwords.py <sentence-splitted, tokenized and lowercased filename>
      """)
      break
    pos_score,neg_score,pos_wc,neg_wc,sequence = get_pos_neg_score(sentence)
    if(interactive):
      print "pos_score,neg_score,pos_wc,neg_wc,sequence: "+str(pos_score)+" : "+str(neg_score)+" : "+str(pos_wc)+" : "+str(neg_wc)+" : "+sequence
    else:
      output.write(str(pos_score)+" : "+str(neg_score)+" : "+str(pos_wc)+" : "+str(neg_wc) + sequence+ '\n')
  if (not(interactive)):
    output.close()
if __name__ == '__main__':
  main()
