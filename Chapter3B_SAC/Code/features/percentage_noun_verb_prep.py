'''
Created on 19-Nov-2013

@author: abhijit
'''
# This program gives the percentage of prepositions in a sentence

import sys
from corenlp import *
from corenlpConfig import corenlp_dir
interactive = False



def count_prepositions_noun_verb_adj(sentence,parseoutput):
    words = parseoutput["sentences"][0]["words"]

    prepositions = 0
    nouns = 0
    verbs = 0
    ad = 0
    for word in words:
        pos = word[1]["PartOfSpeech"]
        if ((pos == 'IN') or (pos == 'TO')): prepositions +=1
        elif (pos[0] == 'N'): nouns +=1
        elif (pos[0] == 'V'): verbs +=1
        elif (pos[0]=="R" or pos[0]=="J"): ad+=1
    total = len(words)

    pn = float(prepositions) / total
    nn = float(nouns) / total
    vn = float(verbs) / total
    adj = float(ad)/(total)
    return [pn,nn,vn,adj]
print >>sys.stderr,"Loading Parser"
corenlp = StanfordCoreNLP(corenlp_dir)
print >>sys.stderr,"Parser Loaded"
while(True):
    im  = raw_input()
    parseoutput = eval(corenlp.parse(im))
    if (im==""):
        break
    print str(count_prepositions_noun_verb_adj(im,parseoutput)[3])
