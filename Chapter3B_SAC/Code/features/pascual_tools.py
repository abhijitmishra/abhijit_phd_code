#!/usr/bin/python
import codecs, string
import sys

"""
Compute the average of a list of numbers.
If the list contains None values, they get filtered out.
If the resulting list is empty, then it returns None.
"""
def average(list_numbers):
  filtered_list = [number for number in list_numbers if number is not None]
  if len(filtered_list) != 0:
    return float(sum(filtered_list)) / len(filtered_list)
  else:
    return 0

"""
Given a filename.idtok, returns two lists (list1, list2), where:
  list1 contains the IDs of every token in the file, and
  list2 contains the tokens themselves.
None of the lists should be empty, adn both should have the same length.
If not, it returns (None, None).
The filename.idtok should have the same format:
1 token1
2 token2
3 token3
. ...
"""
def read_ids_and_tokens(filename):
  finput = codecs.open(filename, 'r', 'utf-8')
  lines = finput.readlines()
  ids = [line.split()[0] for line in lines]
  tokens = [line.split()[1] for line in lines]
  if len(ids) == 0 or (len(ids) != len(tokens)):
    return (None, None)
  return (ids, tokens)

"""
Given a tuple containing two lists (list1, list2), print to stantard output
the elements as follows:
list1[0] list2[0]
list1[1] list2[1]
...      ...
It prints
None None
if the lists are empty or have size mismatchs.
"""
def print_ids_and_features(ids, features):
  if len(ids) == 0 or (len(ids) != len(features)):
    print('None None\n')
    sys.stderr.write('Length of IDs and features_by_word mismatch (%d != %d)\n' % \
                     (len(ids), len(features)))
    return
  for (i, feature) in zip(ids, features):
    print("%d %f" % (int(i), feature))
  return

"""
Given a token, returns True if any character is uppercase.
"""
def contains_uppercase(word):                                                                             
  return len(set(word).intersection(set(string.uppercase))) > 0   
