#!/usr/bin/python

import codecs, os, string, sys
from pascual_tools import *
from nltk.corpus import wordnet as wn
from corenlp import StanfordCoreNLP
from corenlpConfig import corenlp_dir
import math



def compute_passive(sentence,parseoutput):
  N=0
  sent = parseoutput.get('sentences')[0] #Assuming only one sentence
  dep = sent.get("dependencies",[])
  pass_count = 0
  if(dep!=[]):
    pass_count = sum(map(lambda x: 1 if x.__contains__("nsubjpass") else 0,dep))
  return pass_count
  

def main(args = None):
  import textwrap
  interactive = False
  parser = StanfordCoreNLP(corenlp_dir)	
  if len(sys.argv) > 1:
    if sys.argv[1] == "--interactive":
      interactive = True
    
  output = open("passive_clauses", "w")
  while True:
    if interactive:
      sentence = raw_input("Please enter a sentence: ")
    else:
      sentence = raw_input()

    if sentence == "":
      USAGE=textwrap.dedent("""\
      Usage:
          passive.py <sentence-splitted, tokenized and lowercased filename>
      """)
      break
    parseoutput = eval(parser.parse(sentence))
    passive_count = compute_passive(sentence,parseoutput)
    if(interactive):
      print "Passive clause count is "+str(passive_count)
    else:
      output.write(str(passive_count) + '\n')
  if (not(interactive)):
    output.close()

if __name__ == '__main__':
  main()
