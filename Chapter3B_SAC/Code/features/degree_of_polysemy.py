# This program gives the (normalized) degree of polysemy of a sentence

from nltk.corpus import wordnet as wn
import sys

def dp(sentence):
	dp = sum(map(lambda x:len(wn.synsets(x.lower())),sentence.split()))
	dp=dp/len(sentence.split())
	return dp
	
def main():
	interactive = False
	
	if len(sys.argv) > 1:
		if sys.argv[1] == "--interactive":
			interactive = True
	output = open("dp","w")
	while True:
		if interactive:
			sentence = raw_input("Please enter a sentence: ")
		else:
			sentence = raw_input()
	
		if sentence == "":
			break
	
		words = sentence.split()
	
		dp = 0
		for word in words:
			dp +=len(wn.synsets(word.lower()))
	
		dp = float(dp) / len(words)
	
		if interactive:
			print "The (normalized) degree of polysemy is: ", dp
		else:
			output.write(str(dp)+"\n")
	if not(interactive):
		output.close()
if __name__ == '__main__':
  main()
