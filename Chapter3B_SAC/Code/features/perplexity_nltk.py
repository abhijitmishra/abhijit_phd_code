# This program gives the perplexity of a sentence based on a 5-gram model.

import nltk
import sys
import sys
#import cPickle as pickle
import dill
from nltk.model import NgramModel
from nltk.probability import LidstoneProbDist
def build_lm():
	#print "Build Language Model from Brown Corpus"
	brown = nltk.corpus.brown
	corpus = [word.lower() for word in brown.words()]
	train = corpus

	# Remove rare words from the corpus
	fdist = nltk.FreqDist(w for w in train)
	vocabulary = set(map(lambda x: x[0], filter(lambda x: x[1] >= 5, fdist.iteritems())))
	train = map(lambda x: x if x in vocabulary else "*unknown*", train)
	estimator = lambda fdist, bins: LidstoneProbDist(fdist, 0.2) 
	lm = NgramModel(5, train, estimator=estimator)
	return lm
def compute_perplexity(sentence,lm):
	return lm.perplexity(sentence.split())
	
if __name__ == '__main__':
	#print "Building LM"
	lm = build_lm()
	#print "LM built"
	#print "Enter a sentence"
	while True:
		sentence = raw_input()
		if sentence=="":
			break
		print compute_perplexity(sentence,lm)
