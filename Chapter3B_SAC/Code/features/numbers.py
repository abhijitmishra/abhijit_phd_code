#!/usr/bin/python

import codecs, os, sys
from pascual_tools import *

def num_count(sentence):
  digits = []
  digits.extend(\
    map(lambda x: 1 if x.isdigit() else 0, sentence.strip().split()))
  return average(digits)
  
def main(args = None):
  import textwrap
  interactive = False
	
  if len(sys.argv) > 1:
    if sys.argv[1] == "--interactive":
      interactive = True
    
  output = open("numbers_count", "w")
  while True:
    if interactive:
      sentence = raw_input("Please enter a sentence: ")
    else:
      sentence = raw_input()

    if sentence == "":
      USAGE=textwrap.dedent("""\
      Usage:
          numbers.py <tokenized filename>
      """)
      break
    digits = []
    digits.extend(\
      map(lambda x: 1 if x.isdigit() else 0, sentence.strip().split()))
    if(interactive):
      print "Average number count is: "+str(average(digits))
    else:
      output.write(str(average(digits)) + '\n')
  if (not(interactive)):
    output.close()

if __name__ == '__main__':
  main()
