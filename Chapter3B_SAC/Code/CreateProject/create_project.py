# This special purpose program is used to create a number of translog projects taking sentences from a raw corpus.

import sys
import os
import codecs
import random
if not(len(sys.argv)==5):
	print"Insufficient arguments"
	print "create_project.py <Translog Project Template File> <Data File> <Number of Projects to be created> <Output dir>"
	exit(1)

def createProject(database,proj_id,template,FN):
	XMLData = template
	newFN = FN+"\\project"+str(proj_id)+".project"
	sentences="".join(database)
	ID=0
	startPosition=0
	textStartPosition=0
	UnitMarker=""
	for line in database:
		startPosition+=len(line)
		textStartPosition+=len(line)
		UnitMarker+= "<UnitMarker MarkerType=\"USER\" ID=\""+str(ID)+"\" MinDelay=\"1000\" MaxDelay=\"2000\" ClearBeforeProceeding=\"true\" textStartPosition=\""+str(startPosition)+"\" startPosition=\""+str(textStartPosition)+"\" length=\"1\" />\r\n        "
		ID+=1
	UnitMarker=UnitMarker.strip("        ")
	XMLData=XMLData.replace("@@UM@@",UnitMarker)
	XMLData=XMLData.replace("@@FN@@",newFN)
	XMLData=XMLData.replace("@@Sentences@@",sentences)
	with codecs.open(newFN,"w","utf-8")as f:
		f.write(XMLData)

template_file = sys.argv[1]
data_file = sys.argv[2]
N = int(sys.argv[3])
out_dir = sys.argv[4]
FN = os.path.abspath(out_dir)

with codecs.open(template_file,"r","utf-8") as f1:
	template = "".join(f1.readlines())
with codecs.open(data_file,"r","utf-8") as f:
	parent_data_base = map(lambda x:x.strip("\n").strip(),f.readlines())
	databases = [parent_data_base[i:i+N] for i in range(0, len(parent_data_base), N)]
	projectid =0
	for database in databases:
                random.shuffle(database)
                random.shuffle(database)#Two times random shuffling
		projectid += len(database)
		createProject(database,projectid,template,FN)
