# This program ensures selection of uniformly length-distributed subsection from a large corpora

import sys
import random

filename = sys.argv[1]

with open(filename,"r") as f:
	distribution = {}
	data1 = map(lambda x:x.strip("\n"),f.readlines())
	for d in data1:
		l = len(d.split())
		if (l>9 and l<40):
			key = l/5 # buckets like 10-14,15-19,20-24
			lst = distribution.get(key,[])
			lst.append(d)
			distribution[key] = lst
	new_data=[]
	for k in distribution.keys():
		lst = distribution[k]
		l = len(lst)
		m = []
		print "key = "+str(k)+" # = "+str(l)
		if (l<40):
			m = lst
		else:
			random.shuffle(lst)
			random.shuffle(lst)
			m = lst[0:32]
		for i in m:
			new_data.append(i)
	to_write = "\n".join(new_data)

	with open(filename+".new","w") as f1:
		f1.write(to_write)

	
	
