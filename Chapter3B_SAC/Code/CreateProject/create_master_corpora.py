# This program takes input from 

import sys
import random
dir = sys.argv[1]

with open(dir+"/positive.txt","r") as f1:
	d1 = map(lambda x:x.strip("\n").strip("\r\n").strip("\r"),f1.readlines())
	random.shuffle(d1)
	random.shuffle(d1)
with open(dir+"/negative.txt","r") as f2:
	d2 = map(lambda x:x.strip("\n").strip("\r\n").strip("\r"),f2.readlines())
	random.shuffle(d2)
	random.shuffle(d2)
with open(dir+"/objective.txt","r") as f3:
	d3 = map(lambda x:x.strip("\n").strip("\r\n").strip("\r"),f3.readlines())
	random.shuffle(d3)
	random.shuffle(d3)

min_len = min(len(d1),len(d2),len(d3))

lo1 = len(d1)-min_len
lo2 = len(d2)-min_len
lo3 = len(d3)-min_len

to_write=[]
for i in range(min_len):
	to_write.append(d1[i])
	to_write.append(d2[i])
	to_write.append(d3[i])

temp = []
if lo1 >0:
	for element in d1[min_len:]:
		temp.append(element)
if lo2 >0:
	for element in d2[min_len:]:
		temp.append(element)
if lo3 >0:
	for element in d3[min_len:]:
		temp.append(element)

if (not(temp==[])):
	random.shuffle(temp)
	random.shuffle(temp)
	random.shuffle(temp)
	for m in temp:
		to_write.append(m)

material = "\n".join(to_write)

with open("master_file.txt","w") as f4:
	f4.write(material)