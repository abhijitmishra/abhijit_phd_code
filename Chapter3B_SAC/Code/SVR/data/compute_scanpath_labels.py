import csv

def avg(l):
	return float(sum(l))/float(len(l))
reader = csv.reader(open("scanpath_data.csv"))

doc_data = {}
header = 1
for row in reader:
	if header==1:
		header=0
		continue
	pid = row[0]
	did = int(row[1])
	val = row[2]
	if "NUM" in val:
		val=1.0
	ddata = doc_data.get(did,[])
	ddata.append(float(val))
	doc_data[did] = ddata
out = open("avg_scanpath_data.csv","w")
ks = doc_data.keys()
ks.sort()

for k in ks:
	out.write(str(k)+","+str(avg(doc_data[k]))+"\n")
