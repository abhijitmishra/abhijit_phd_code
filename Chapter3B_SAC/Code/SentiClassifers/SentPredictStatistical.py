import sys
import math
import pickle
import nltk
from nltk import NaiveBayesClassifier
from sklearn.svm import NuSVC
from nltk.classify.maxent import MaxentClassifier
from nltk.classify.scikitlearn import SklearnClassifier
from sklearn.feature_extraction import DictVectorizer
from nltk.util import ngrams
from nltk.classify.util import accuracy
from nltk.corpus import wordnet as wn
import random
import getopt
import os
def twitterNormalize(sentence):
	return sentence
def formatData(sentence,morph):
	data = {}
	sentence = sentence.lower()
	sentence = twitterNormalize(sentence)
	words = sentence.split()
	unigrams = set(words)
	bigrams = map(lambda x: " ".join(list(x)),ngrams(words,2))
	bigrams = set(bigrams)
	trigrams = []
	if (len(words)>2):
		trigrams = map(lambda x: " ".join(list(x)),ngrams(words,3))
		trigrams = set(trigrams)
	for i in unigrams:
		data[i] = 1
	for j in bigrams:
		data[j] = 1
	for k in trigrams:
		data[k] = 1
	if(morph):
		#Add N-gram lemmas into feature list
		morphedSentence = " ".join(map(lambda x:x if wn.morphy(x) is None else wn.morphy(x),sentence.split()))
		words = morphedSentence.split()
		unigrams = set(words)
		bigrams = map(lambda x: " ".join(list(x)),ngrams(words,2))
		bigrams = set(bigrams)
		trigrams = []
		if (len(words)>2):
			trigrams = map(lambda x: " ".join(list(x)),ngrams(words,3))
			trigrams = set(trigrams)
		for i in unigrams:
			data[i] = 1
		for j in bigrams:
			data[j] = 1
		for k in trigrams:
			data[k] = 1	
	return data

def trainMaxent(train):
	maxEntClassifier = MaxentClassifier.train(train, algorithm="iis")
	return maxEntClassifier

def trainSVM(train):
	sc =SklearnClassifier(NuSVC(probability=True))
	SVMClassifier = sc.train(train)
	return SVMClassifier

def trainNB(train):
	NBClassifier = NaiveBayesClassifier.train(train)
	return NBClassifier
	
def test(classifier,test,tp):
	#remove labels first
	test = map(lambda x:x[0],test)
	maxProb = []
	p =[] 
	p = classifier.batch_classify(test)
	probDists = classifier.batch_prob_classify(test)
	for x in probDists:
		maxProb.append(max([x.prob('1'),x.prob('-1')]))
	print p
	print maxProb
	predictions = zip(p,maxProb)
	return predictions

def getAccuracy(classifier,dev):
	return accuracy(classifier,dev)

def k_fold_cross_validation(items, k, randomize=True):
	fold = []
	fold_accuracy_maxEnt = 0	
	fold_accuracy_SVM = 0
	fold_accuracy_NB = 0
	#print items
	if randomize:
		items = list(items)
		random.shuffle(items)

	slices = [items[i::k] for i in xrange(k)]
	#print len(slices)
	for i in xrange(k):
		validation = slices[i]
		
		training = [item
                    for s in slices if s is not validation
                    for item in s]
		#print len(training),len(validation)
        	fold.append([training, validation])
	print len(fold)
	fold_id = 1
	for f in fold:	
		tr = f[0]
		val = f[1]
		print "Training MaxEnt for fold:"+ str(fold_id )
		mxClassifier = trainMaxent(tr)
		print "Training SVM for fold:"+ str(fold_id)
		svmClassifier = trainSVM(tr)
		print "Training NB for fold:"+ str(fold_id) 
		nbClassifier = trainNB(tr)
		accMx = getAccuracy(mxClassifier,val)
		#print val
		accSVM = getAccuracy(svmClassifier,val)
		accNB = getAccuracy(nbClassifier,val)
		fold_accuracy_maxEnt+= accMx
		print "Accuracy MaxEnt for fold:"+ str(fold_id) +" is-> "+str(accMx)
		fold_accuracy_SVM += accSVM
		print "Accuracy SVM for fold:"+ str(fold_id) +" is-> "+str(accSVM)
		fold_accuracy_NB += accNB
		print "Accuracy NB for fold:"+ str(fold_id) +" is-> "+str(accNB)
		fold_id +=1

	return[float(fold_accuracy_maxEnt)/k,float(fold_accuracy_SVM)/k,float(fold_accuracy_NB)/k]

	
def readFromFile(fn,morph):
	data = []
	#Files with format: <Sentences>@@@<sentLabel in -1/0/1 format>
	with open(fn,"r") as f:
		lines =map(lambda x:x.strip("\n"),f.readlines())
		for l in lines:
			inf = l.split("@@@")
			if(len(inf)==2):
				sent = inf[0].strip()
				label = inf[1].strip()
				da = formatData(sent,morph)
				data.append((da,label))					
	return data

def usage():
	print >> sys.stderr , "Usage: SentiPredictStatistical.py\n \
		-C <command, k-fold/test>\n\
		-D datafile in the format of <sentence>@@@<sentiLabels -1/0/1 for -ve/obj/+ve>\n\
		-T training data file in case of \"test\" command\n \
		-E test data file in case of \"test\" command \n\
		-N number of folds in case of \"k-fold\" command\n\
		-M for switching off/on morphological analysis\n\
		-R for classifiers (MaxEnt,SVM,NB)\n\
		-h this help"

if __name__ == "__main__":
	cmd=""
	dataFile = ""	
	trainingFile = ""
	testFile = ""
	morph = False
	folds = ""
	trial = ""
	
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hC:D:E:T:N:R:M", ["help", "output="])
    	except getopt.GetoptError as err:
		# print help information and exit:
		print >>sys.stderr,str(err) # will print something like "option -a not recognized"
		usage()
		sys.exit(2)
	for o,a in opts:
		if o=="-C":
			cmd = a
		elif o=="-D":
			dataFile = a
		elif o=="-T":
			trainingFile = a
		elif o=="-E":
			testFile = a
		elif o=="-N":
			folds = int(a)
		elif o=="-M":
			morph==True
		elif o=="-R":
			trial = a
		elif o=="-h":
			usage()
			exit(1)

	if (not(cmd.__contains__("k-fold")) and not(cmd.__contains__("test"))):	
		print >>sys.stderr, "Invalid command. Should select from k-fold/test.\n"
		usage()
		exit(1)
	if cmd.__contains__("k-fold"):
		data=readFromFile(dataFile,morph)		
		k_fold_cross_validation(data,folds)
	if cmd.__contains__("test"):
		data_train = readFromFile(trainingFile,morph)
		data_test = readFromFile(testFile,morph)
	if (trial=="MaxEnt"):
		if(os.path.isfile("models/MaxEntSubjective.model")):
			print "Model file found for MaxEnt. No need to train. Loading MaxEnt to memroy..."
			mxClassifier = pickle.load(open("models/MaxEntSubjective.model","r"))
		else:
			print "Training MaxEnt"
			mxClassifier = trainMaxent(data_train)
			pickle.dump(mxClassifier,open("models/MaxEntSubjective.model","w"))
		print "Testing MaxEnt..."
		mxResults = test(mxClassifier,data_test,"MaxEnt")			
		print "Accuracy MaxEnt..."
		mxAccuracy = getAccuracy(mxClassifier,data_test)
		print mxAccuracy
	elif (trial=="SVM"):
		if(os.path.isfile("models/SVMSubjective.model")):	
			print "Model file found for SVM. No need to train. Loading SVM to memroy..."
			svmClassifier = pickle.load(open("models/SVMSubjective.model","r"))
		else:
			print "Training SVM"
			svmClassifier = trainSVM(data_train)
			pickle.dump(svmClassifier,open("models/SVMSubjective.model","w"))
		print "Testing SVM..."
		svmResults = test(svmClassifier,data_test,"NB")
		print "Accuracy SVM..."
		svmAccuracy = getAccuracy(svmClassifier,data_test)
		print svmAccuracy
	
	elif (trial=="NB"):
		if(os.path.isfile("models/NBSubjective.model")):
			print "Model file found for NB. No need to train. Loading NB to memroy..."
			nbClassifier = pickle.load(open("models/NBSubjective.model","r"))
		else:
			print "Training NB"
			nbClassifier = trainNB(data_train)
			pickle.dump(nbClassifier,open("models/NBSubjective.model","w"))
		print "Testing NB..."
		nbResults = test(nbClassifier,data_test,"NB")
		print "Accuracy NB..."
		nbAccuracy = getAccuracy(nbClassifier,data_test)
		print nbAccuracy
	
	
	
	tw = "Output\tConfidence\n"
	res = []
	acc = 0
	if trial=="SVM":
		res = svmResults
		acc = svmAccuracy
	elif trial=="NB":
		res = nbResults
		acc = nbAccuracy
	elif trial=="MaxEnt":
		res = mxResults
		acc = mxAccuracy		 
	for i in range(len(res)):
		tw+= str(res[i][0])+"\t"+str(res[i][1])+"\n"
	tw+="Accuracy\t"+str(acc)
	with open("results"+trial+".txt","w") as m:
		m.write(tw)

