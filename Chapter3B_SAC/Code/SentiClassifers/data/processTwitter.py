#Process the tr file
import codecs
import csv

subjData = open("subjective.csv","w")
polarityData  =open("polarity.csv","w")
lmFile = open("twitterLM","w")
with open("tr","r") as f:
	reader = csv.reader(f, delimiter=',', quotechar='"')
	posCount = 0
	negCount = 0
	objCount = 0
	for row in reader:
		label = row[0]
		text = row[-1]
		lmFile.write(text.strip("\n").strip()+"\n")
		if label=="2":
			#Objective
			objCount+=1
			subjData.write(text.strip()+"@@@"+"-1\n")
		elif label=="0":
			#Negative
			negCount+=1
			subjData.write(text.strip()+"@@@"+"1\n")
			polarityData.write(text.strip()+"@@@"+"-1\n")
		elif label=="4":
			#Positive
			posCount+=1
			subjData.write(text.strip()+"@@@"+"1\n")
			polarityData.write(text.strip()+"@@@"+"1\n")
print negCount
print objCount
subjData.close()
polarityData.close()
lmFile.close()
