#Process the tr file
import codecs
import csv
import sys
polarityData  =open("polarityMovie.csv","w")
lmFile = open("movieLM","w")
with open("mo.csv","r") as f:
	reader = csv.reader(f, delimiter=',', quotechar='"')
	posCount = 0
	negCount = 0
	objCount = 0
	for row in reader:
		label = row[0]
		text = row[-1]
		lmFile.write(text.strip("\n").strip()+"\n")
		if label=="1":
			#Negative
			negCount+=1
			polarityData.write(text.strip()+"@@@"+"-1\n")
		elif label=="5":
			#Positive
			posCount+=1
			polarityData.write(text.strip()+"@@@"+"1\n")
print negCount
print posCount
polarityData.close()
lmFile.close()
