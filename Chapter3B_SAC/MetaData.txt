Sentence Complexity experiment:
Eye-movement was recorded. Values obtained: Fixations, Saccades, Pupil Dialation, Regression counts,Fixation Counts, Fixation-Word Mapping
Eye-tracker:Tobii TX300 (300Hz Binoccular Sampling rate)
Participants: 4 male one female, age [20-24], TOEFL > 110 (Good).
1059 Sentences were annotated (566 Movie,493 Tweets from General Tweet domain)

Instruction to participant: We should refer to Aadi's document.
Labels annotated: SP,P,O,N,SN,CS (can't say). Later zipped to three labels (P,N,O)
Time taken for total experiment completion: 4-5 hrs (for one participant). Frequent breaks were taken to avoid fatigue.

Sentence Collection for sentiment complexity experiment:

Sentiment Complexity corpora:
	1: Movie domain:
		http://www.cs.cornell.edu/people/pabo/movie-review-data/ for both objective and polar sentence selection (cite Pang and Lee)
	2: Twitter Domain:
		http://help.sentiment140.com/for-students (cite Sentiment 140):(Test data was taken)
	
	For eye tracking: (P/N/O)
		Movie: Gold (192,192,192)
		Movie: Consensus(256,251,59)
		Twitter: Gold(181,175,137)
		Twitter Conensus(188,150,155)

Multirated kappa statistics:
(REF: Siegel and Castellan, "Nonparametric Statistics for the
	   Behavioral Sciences", 2nd Ed., McGraw-Hill Book Co., 1988
	   Sec. 9.8, pp 284-291)
Computed multirated kappa for 5 Participants:
	Multi Kappa=0.6867069826 (Good Agreement)	Z=91.1160696274		PA=0.7963172805	PE=0.3498651159
	(http://adorio-research.org/wordpress/?p=2307)

From the consensus data: Anotation confusion score was computed (which may be an indicator of complexity):
	(for opposite polarity annotation mistake, we penalize more)
	confidence = #N*2+#O if Consensus=P;#P*2+#O if Consensus = N; #P+#N if Consensus = O


Potential Labels for SCI: Average FixDur, Regressions, Total annotation time, Manual labeling (fraught with subjectivity)
Why "total time taken (TT)" was used as a label (Since it may have been affected by environmental distrcations) : 
	1: Intuitively, Time taken is a good measure for complexity.
	2: FixDur, TT for annotation are well corrlelated (0.98 avg). TT is positively correlated with regressions (0.45) and the above mentioned confusion score (0.3).
	3: If TT is full-proof, we do not need expensive eye-tracking set-up; which makes the set up well replicable. One just has to record the total time taken for any SA annotation task (be it document level or sentence level or microblogs).

