Instructions:

Create a new file `Code/features/corenlpConfig.py` with the following line
corenlp_dir = "<absolute path to stanford corenlp directory contain the JAR>"

Directory Structure:

	Code: Contains codes for 1. creating Translog project files and 2. evenly distributing positive,negative and objective 
	      sentences for each experiment sessions. In each session there will same number of +ve,-ve and obj sentences will be
	      presented but in random order.
	Corpora: Contains raw sentences from Movie and Twitter databases in the respective directories. Distribution.txt contains the 
	      counts of sentences for each unique category .
	Recorded_Data: It is empty as of now. It will contain recorded data in XML format.
	Translog_Projects: It contains 22 translog project files. The have been tested them in translog.
