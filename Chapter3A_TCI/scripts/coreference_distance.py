#!/usr/bin/python

import codecs, os, string, sys
from pascual_tools import *
from nltk.corpus import wordnet as wn
from corenlp import StanfordCoreNLP
from corenlpConfig import corenlp_dir
import math



def compute_coref_distance(sentence,parsed):
    N=0
    coref = parsed.get("coref",[])
    dist = 0
    if(coref!=[]):
      for sent in coref:
	for c_rel in sent:
            dist+=abs(c_rel[0][4]-c_rel[1][4])
	    N+=1
      return float(dist/N)
    return dist
  

def main(args = None):
  import textwrap
  interactive = False
  parser = StanfordCoreNLP(corenlp_dir)
  
  if len(sys.argv) > 1:
    if sys.argv[1] == "--interactive":
      interactive = True
  output = open("correferrence_distance", "w")
  while True:
    if interactive:
      sentence = raw_input("Please enter a sentence: ")
    else:
      sentence = raw_input()

    if sentence == "":
      USAGE=textwrap.dedent("""\
      Usage:
          correferrence_distance.py <sentence-splitted, tokenized and lowercased filename>
      """)
      break
    parsed = eval(parser.parse(sentence))
    dist = compute_coref_distance(sentence,parsed)
    if(interactive):
      print "Average co-referrence distance is "+str(dist)
    else:
      output.write(str(dist) + '\n')
  if (not(interactive)):
    output.close()

if __name__ == '__main__':
  main()
