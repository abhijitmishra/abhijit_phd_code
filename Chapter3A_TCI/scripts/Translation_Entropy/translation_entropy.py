#Calculate translation Entropy
#Author Abhijit
import sys
import gzip
import codecs
import math
import itertools
from multiprocessing import Pool,freeze_support

phrase_table = {}
def load_phrase_table(phrase_table_path):
	#loads phrase tables to memory. Phrases and there probabilities are loaded
	#phrase_table = {}
	zf = gzip.open(phrase_table_path, 'rb')
	reader = codecs.getreader("utf-8")
	content = reader( zf )
	for line in content:
		line_info = line.split("|||")
		phrase = line_info[0].strip()
		direct_translation_probability = float(line_info[2].split()[2].strip())
		#for each phease maintain a probability list
		plist_for_phrase = phrase_table.get(phrase,[])
		plist_for_phrase.append(direct_translation_probability)
		phrase_table[phrase] = plist_for_phrase
		
	#print >>sys.stderr, "Phrase table loaded..."
	zf.close()
	#return phrase_table
def chunks(l, n):
    """ Yield successive n-sized chunks from l.
    """
    for i in xrange(0, len(l), n):
        yield l[i:i+n]

def find_all_partitions(text):
    words = text.split()
    my_chain = []
    if(len(words)>15):
		#the partitioning is exponential to number of words. so spliting has to be done
        chu = (len(words)/15)+1
        for k in chunks(words,15):
            for ch in find_all_partitions(" ".join(k)):
                yield ch
    else:
        ns = range(1, len(words)) # n = 1..(n-1)
        for n in ns:
            for idxs in itertools.combinations(ns, n):
                yield [' '.join(words[i:j]) for i, j in zip((0,) + idxs, idxs + (None,))]
            
def compute_entropy_phrase(phrase):
	phrase_probabilities = phrase_table.get(phrase,[])
	if phrase_probabilities == []:
		entropy = 100.0 #0 if the phrase is not found, assign higher entropy
	else:
		#compute entropy for a phrase
		entropy = sum(map(lambda x:(-x)*math.log(x,2),phrase_probabilities))
	#Compute average entropy  = total_phrase_entropy/number of words in a phrase
	return entropy/len(phrase.split())

def c_star(partition):
	return sum([compute_entropy_phrase(phrase) for phrase in partition])

def get_translation_entropy_sentence(src_sentence):
	#print src_sentence
	#contains entropy for each possible partition
	wc = len(src_sentence.split())
	result_entropy_sequence=[]
	partitions = find_all_partitions(src_sentence)
	partition_count=1000
	for partition in partitions:
		###########Pruning##########
		#Comment if you want searching over the whole space
		# If a single phrase is larger then one third of the whole sentence, prune it
		
		if wc>30:
			if max(map(lambda phrase:len(phrase.split()),partition))>(wc/15):
				continue
		elif wc>20:
			if max(map(lambda phrase:len(phrase.split()),partition))>(wc/10):
				continue
		elif wc>15:
			if max(map(lambda phrase:len(phrase.split()),partition))>(wc/7):
				continue
		
		result_entropy_sequence.append(c_star(partition))
		############################
	if result_entropy_sequence==[]:
		return 100 #a very large entropy
	H = min(result_entropy_sequence) # minimum entropy is selected
	#print "sentence_enitropy: "+str(H)
	return H

def get_translation_entropy_corpus(src_file,proc):
	#ensure multithreading
	entropy = 0.0
	pool = Pool(processes = proc)
	with codecs.open(src_file,"r","utf-8") as f:
		data = f.readlines()[:500]
		result = pool.map(get_translation_entropy_sentence,data)
		#print len(result)
		#print len(data)
		entropy = sum(result)/len(data)
	pool.close()
	pool.join()
	return entropy
def main():
	#Main function
	if len(sys.argv)!=5:
		print "Usage: translation entropy.py <test_src_file_path> <phrase_table_path> <language pair: en-hi><no of cores>"
                exit(0)
	src_file = sys.argv[1]
	phrase_table_path = sys.argv[2]
	lang_pair = sys.argv[3]
        proc = int(sys.argv[4])
	load_phrase_table(phrase_table_path)

	with codecs.open(src_file,"r","utf-8") as f:
		for ln in f:
			ln = ln.strip("\n")
			print get_translation_entropy_sentence(ln)
	#avg_entropy = get_translation_entropy_corpus(src_file,proc)
	#print lang_pair+":"+str(avg_entropy)
	
if __name__ == "__main__":
	freeze_support()
	main()
