#Calculate translation Entropy
import sys
import gzip
import codecs
import math
import itertools
from multiprocessing import Pool,freeze_support

def load_phrase_table(phrase_table_path):
	#loads phrase tables to memory. Phrases and there probabilities are loaded
	phrase_table = {}
	zf = gzip.open(phrase_table_path, 'rb')
	reader = codecs.getreader("utf-8")
	content = reader( zf )
	for line in content:
		line_info = line.split("|||")
		phrase = line_info[0].strip()
		direct_translation_probability = float(line_info[2].split()[2].strip())
		#for each phease maintain a probability list
		plist_for_phrase = phrase_table.get(phrase,[])
		plist_for_phrase.append(direct_translation_probability)
		phrase_table[phrase] = plist_for_phrase
		
	print "Phrase table loaded..."
	zf.close()
	return phrase_table
	
def find_all_partitions(text):
    words = text.split()
    ns = range(1, len(words)) # n = 1..(n-1)
    for n in ns:
        for idxs in itertools.combinations(ns, n):
            yield [' '.join(words[i:j]) for i, j in zip((0,) + idxs, idxs + (None,))]
            
def compute_entropy_phrase(phrase,phrase_table):
	phrase_probabilities = phrase_table.get(phrase,[])
	if phrase_probabilities == []:
		entropy = 0.0 #0 if the phrase is not found
	else:
		#compute entropy for a phrase
		entropy = sum(map(lambda x:(-x)*math.log(x,2),phrase_probabilities))
	#Compute average entropy  = total_phrase_entropy/number of words in a phrase
	return entropy/len(phrase.split())
def temp_return(x,y):
	#Just to unzip the values
	return [x,y]
def c_star(a_b):
	#This is just for multithreading using pool.map that does not accept functions with more than one arguements
	# equivalent to converting `f([1,2])` to `f(1,2)` call.
	phrases, pt = temp_return(*a_b)
	return sum(map(lambda phrase:compute_entropy_phrase(phrase,pt),phrases))

def get_translation_entropy_sentence(src_sentence,phrase_table,pool):
	# Parallel processing ensured here as well.
	entropy_list = [] #contains entropy for each possible partition
	wc = len(src_sentence.split())
	partition_list = []
	for partition in find_all_partitions(src_sentence):
		###########Pruning##########
		#Comment if you want searching over the whole space
		# If a single phrase is larger then half of the whole sentence, prune it
		if wc>10:
			if max(map(lambda phrase:len(phrase.split()),partition))>(wc/2):
				continue
		partition_list.append(partition)
		############################
	result_entropy_sequence = pool.map(c_star, itertools.izip(partition_list, itertools.repeat(phrase_table)))
	if result_entropy_sequence==[]:
		return 100 #a very large entropy
	H = min(result_entropy_sequence) # minimum entropy is selected
	print "sentence_entropy"+str(H)
	return H


def get_translation_entropy_corpus(src_file,phrase_table,proc):
	#ensure multithreading
	entropy = 0.0
	with codecs.open(src_file,"r","utf-8") as f:
		data = f.readlines()
		pool = Pool(processes=proc)
		result = map(lambda s:get_translation_entropy_sentence(s,phrase_table,pool),data)
		entropy = sum(result)/len(data)
	return entropy
	
def main():
	#Main function
	if len(sys.argv)!=4:
		print "Usage: translation entropy.py <test_src_file_path> <phrase_table_path> <no of cores>"
	src_file = sys.argv[1]
	phrase_table_path = sys.argv[2]
	proc = int(sys.argv[3])
	phrase_table = load_phrase_table(phrase_table_path)
	
	avg_entropy = get_translation_entropy_corpus(src_file,phrase_table,proc)
	print avg_entropy
	
if __name__ == "__main__":
	freeze_support()
	main()
