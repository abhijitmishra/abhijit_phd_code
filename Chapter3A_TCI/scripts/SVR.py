'''
Created on 10-Nov-2013

@author: abhijit
'''
import codecs
from numpy import min,max
from numpy import array
import svmlight
import sys
import pickle
import math
from random import shuffle

def getNormalizeSingle(imin,imax,minMax,val):
    #For single valued. This is used from testing one sentence at a time.
    dmin = minMax[0]
    dmax = minMax[1]
    normalized = imin + (imax - imin)*(val - dmin)/(dmax - dmin)
    return normalized
def getNormalizedValues(imin,imax,minMax,arr):
    #Normalize the whole array
    """I = Imin + (Imax-Imin)*(D-Dmin)/(Dmax-Dmin)"""
    dmin = minMax[0]
    dmax = minMax[1]
    normalized = imin + (imax - imin)*(arr - dmin)/(dmax - dmin)
    return normalized

def writeMinMaxValues(f,fPath):
    with open(fPath, "w") as fl:
        pickle.dump(f, fl)
def readMinMaxValues(fPath): 
    with open(fPath, "r") as fl:
        return pickle.load(fl)

def normalizeData(filePath,flag):
    dataDict = {}
    with codecs.open(filePath,"r","utf-8") as f:
        reader = map(lambda z:z.split("\t"),map(lambda x:x.strip("\n"),f.readlines()))
        trans = zip(*reader)
        for t in trans:
            dataDict[t[0].strip()] = t[1:]
        dataDict.pop("Sentence")
        keys = dataDict.keys()
        keys.sort()
        
        if flag =="training":
            minMax = {}
            for k in keys:
                arr = array(map(lambda x:float(x),dataDict[k]))
                min1 = min(arr)
                max1 = max(arr)
                minMax[k] = [min1,max1]
            writeMinMaxValues(minMax,"../data/minMax.dat")
        normDataDict = {}
        minMax = readMinMaxValues("../data/minMax.dat")
        
        for k in keys:
            normDataDict[k] = getNormalizedValues(0,1,minMax[k],array(map(lambda x:float(x),dataDict[k])))
        normLabels = map(lambda x:x*10,normDataDict['Tp']) #Scaling the label in the range of 1 to 10
        normFeatures = map(lambda x:normDataDict[x] if x!="Tp" else None,normDataDict)
        i =0
        finalData = []
        for l in normLabels:
            featureIndex = 1
            featureList = []
            for feature in normFeatures:
                if(feature!=None):
                    featureList.append((featureIndex,feature[i]))
                    featureIndex+=1
            t = (l,featureList)
            finalData.append(t)         
            i+=1
    return [normLabels,finalData]

'''
def loadData(filePath,flag):
    with codecs.open(filePath,"r","utf-8") as f:
        data = map(lambda x:x.strip("\n"),f.readlines()) #excluding header
        header = data[1:]
        data = data[1:]
        tuples = map(lambda x:x.split("::"),data)
        sSet = zip(*tuples)  #Transpose
        fSet = sSet[2:] #Excluding sentences All features present)
        lSet = sSet[1]
        print "....................."
        if (flag == "training"):
            f = {}
        #print lSet
        normLabels = getNormalizedValues(0,1,array(map(lambda x:float(x),lSet)))
        normFeatures = map(lambda elemt:getNormalizedValues(0,1,array(map(lambda x:float(x),elemt))),fSet)
        i =0
        finalData = []
        for l in normLabels:
            featureIndex = 1
            featureList = []
            for feature in normFeatures:
                featureList.append((featureIndex,feature[i]))
                featureIndex+=1
            t = (l,featureList)
            finalData.append(t)
            
            i+=1
    return [normLabels,finalData]
'''
def train(trainingData):
    model = svmlight.learn(trainingData, type='regression',C=20,kernel="polynomial",poly_deg=2, verbosity=0)
    svmlight.write_model(model, '../data/model.dat')
def test(testData):
    model = svmlight.read_model('../data/model.dat')
    predictions = svmlight.classify(model, testData)
    return predictions
def MSE(orig,pred):
    #Mean Squared error computation
    
    orig = map(lambda x:list(x)[0],orig)
    #print orig
    #print pred
    mse = 0
    mse = sum(map(lambda x:math.pow((orig[x]-pred[x]),2)+mse,range(len(orig))))
    mse = math.sqrt(mse)/len(orig)
    return mse

def k_fold_cross_validation(items, k, randomize=False):
    fold = []
    total_mse  = 0
    if randomize:
        items = list(items)
        shuffle(items)

    slices = [items[i::k] for i in xrange(k)]

    for i in xrange(k):
        validation = slices[i]
        training = [item
                    for s in slices if s is not validation
                    for item in s]
        fold.append([training, validation])
    for f in fold:
        train(f[0])
        pred = test(f[1])
        mse = MSE(f[1],pred)
        print "Error in fold: " + str(mse)
        total_mse+=MSE(f[1],pred)
    print "Total mse: "+ str(float(total_mse)/k)

def main():
    trData = sys.argv[1]
    k = sys.argv[2]
    data =normalizeData(trData,"training")[1]
    #train(data)
    #Uncomment the following code for K-fold evaluation    
    k_fold_cross_validation(data,int(k))
if __name__ == "__main__":
    main()
