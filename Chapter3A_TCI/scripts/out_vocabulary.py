#!/usr/bin/python

import codecs, os, string, sys
from pascual_tools import *
from nltk.corpus import wordnet as wn

base_path = os.path.dirname(os.path.abspath(__file__))
"""
Load the dictionary containing the words in the General Listing Service (GSL).
"""
finput_gsl = codecs.open(base_path+'/../resources/gsl.txt', 'r', 'utf-8')
gsl_word_lines = finput_gsl.readlines()
gsl_words = map(lambda x: x.strip(), gsl_word_lines)
"""
Load the dictionary containing the words in the Academic Word List (AWL).
"""
finput_awl = codecs.open(base_path+'/../resources/awl.txt', 'r', 'utf-8')
awl_word_lines = finput_awl.readlines()
awl_words = map(lambda x: x.strip(), awl_word_lines)

"""
Check if a word (or its lemma) are not punctuations and whether they are in
the General Listing Service or Academic Word List.
"""
def word_in_gsl_or_awl(word):
  global gsl_words
  global awl_words
  if word in string.punctuation:
    return None
  lemma = wn.morphy(word)
  if lemma is None:
    lemma = word
  return lemma not in gsl_words and lemma not in awl_words
  
def out_vocabulary(sentence):
  is_word_in_list = []
  is_word_in_list.extend(\
    map(lambda x: word_in_gsl_or_awl(x), sentence.strip().split()))
  return average(is_word_in_list)
  
def main(args = None):
  import textwrap
  interactive = False
	
  if len(sys.argv) > 1:
    if sys.argv[1] == "--interactive":
      interactive = True
    
  output = open("out_of_vocabulary", "w")
  while True:
    if interactive:
      sentence = raw_input("Please enter a sentence: ")
    else:
      sentence = raw_input()
    sentence = sentence.decode("utf-8").lower()
    if sentence == "":
      USAGE=textwrap.dedent("""\
      Usage:
          out_vocabulary.py <sentence-splitted, tokenized and lowercased filename>
      """)
      break
    is_word_in_list = []
    is_word_in_list.extend(\
      map(lambda x: word_in_gsl_or_awl(x), sentence.strip().split()))
    if(interactive):
      print "Academic vocabulary content is: "+str(average(is_word_in_list))
    else:
      output.write(str(average(is_word_in_list)) + '\n')
  if (not(interactive)):
    output.close()
if __name__ == '__main__':
  main()
