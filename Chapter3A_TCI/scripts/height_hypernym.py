#!/usr/bin/python

import codecs, os, sys
import nltk
from nltk.corpus import wordnet as wn
from pascual_tools import *

"""
Compute the height of the hypernyms above the lemmatized word.
There might be several hypernyms paths to the root. In that case,
the average is computed.
"""
def height_hypernym(word):
  lemma = wn.morphy(word)
  if lemma is None:
    return None
  synsets = wn.synsets(lemma)
  if (len(synsets) == 0):
    return None
  hyp = lambda x: x.hypernyms()
  hypernyms_lengths = map(len, map(lambda x: list(x.closure(hyp)), synsets))
  return average(hypernyms_lengths)

def height_hypernym_sentence(sentence):
  height_hypernym_per_word = []
  height_hypernym_per_word.extend(\
    map(height_hypernym, sentence.lower().strip().split()))
  return average(height_hypernym_per_word)
  
def main(args = None):
  import textwrap
  interactive = False
	
  if len(sys.argv) > 1:
    if sys.argv[1] == "--interactive":
      interactive = True
    
  output = open("height_hypernym", "w")
  while True:
    if interactive:
      sentence = raw_input("Please enter a sentence: ")
    else:
      sentence = raw_input()
    sentence = sentence.decode("utf-8").lower()
    if sentence == "":
      USAGE=textwrap.dedent("""\
      Usage:
          height_hypernym.py <sentence-splitted, tokenized and lowercased>
      """)
      break
    height_hypernym_per_word = []
    height_hypernym_per_word.extend(\
      map(height_hypernym, sentence.lower().strip().split()))
    if(interactive):
      print "Average height of hypernym content is: "+str(average(height_hypernym_per_word))
    else:
      output.write(str(average(height_hypernym_per_word)) + '\n')
  if (not(interactive)):
    output.close()
if __name__ == '__main__':
  main()
