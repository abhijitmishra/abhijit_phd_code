from all_features import all_features
from nltk.tokenize.punkt import PunktWordTokenizer
import svmlight
from SVR import readMinMaxValues
from SVR import getNormalizedValues
from numpy import array
import os
# Create your models here.
DIR = os.path.abspath(os.path.dirname(__file__))

# Load the minimum and maximum values of the features and the labels
minMax = readMinMaxValues(DIR+"/../data/minMax.dat")

def normalize_text(text, trial):
    # Add normalization code to remove noise (Unicode chars, duplications etc.)
    return text

def request_tci(features):
    # First Normalize the text
    #text = normalize_text(text)
    keys = features.keys()
    prediction=0
    keys.sort()
    normFeatures = {}
    for k in keys:
        normFeatures[k] = getNormalizedValues(0,1,minMax[k],array(features[k]))
   	
	# Construct tuples to feed into SVMlight
    tuple1 = map(lambda x:normFeatures[x],keys)
    tuple1 = map(lambda y:((y+1),tuple1[y]),range(len(tuple1)))
    completeTuple = [(0,tuple1)] #default label is 0
    
    model = svmlight.read_model(DIR+'/../data/model.dat') #If you are training by yourself, point to the model file here
    prediction = svmlight.classify(model, completeTuple)[0]
    return prediction

def request_features(sentence,tgt_language):
    features = all_features(sentence,tgt_language)
    return features

def main():
    while(True):
        print "Enter the source sentence. Input blank line to terminate"
        sentence = raw_input()
        if sentence=="":
            break
        print "Enter the target language" #As of now it has nothing to do with the result. Just a dummy.
        language = raw_input()
        features = request_features (sentence,language)
        tci =  request_tci(features)
        print "TCI = "+str(tci)

if __name__=="__main__":
    main()
