#!/usr/bin/python

import codecs, os, sys
import nltk
from pascual_tools import *

'''
The ne_chunk function returns a flat tree.
If a child of such a tree contains the attribute "node",
then it is a named entity.
We return the average number of named entities per chunk.
'''
def named_entities(tokenized_sentence):
  tree = nltk.ne_chunk(nltk.tag.pos_tag(tokenized_sentence))
  return len([child.node for child in tree if hasattr(child, 'node')])

def main(args = None):
  import textwrap
  interactive = False
	
  if len(sys.argv) > 1:
    if sys.argv[1] == "--interactive":
      interactive = True
  output = open("named_entity", "w")
  while True:
    if interactive:
      sentence = raw_input("Please enter a sentence: ")
    else:
      sentence = raw_input()

    if sentence == "":
      USAGE=textwrap.dedent("""\
      Usage:
          named_entity.py <sentence-splitted, tokenized and lowercased filename>
      """)
      break
    named_entities_per_sentence = named_entities(sentence.split())
    if(interactive):
      print "Average named entity score is: "+str(named_entities_per_sentence)
    else:
      output.write(str(named_entities_per_sentence) + '\n')
  if (not(interactive)):
    output.close()


if __name__ == '__main__':
  main()
