# This program gives the dependency density of a sentence

import sys
from corenlp import StanfordCoreNLP
from corenlpConfig import corenlp_dir
interactive = False

def terminal_non_terminal_ratio(sentence,parseoutput):

	parsetree = parseoutput["sentences"][0]["parsetree"]
	terminals = len(parseoutput["sentences"][0]["words"])
	non_terminals = 0
	for c in parsetree:
		if (c == '('): 
			non_terminals += 1
	non_terminals -= terminals
	dd = float(non_terminals) / terminals
	
	return dd
	
def main():
	if len(sys.argv) > 1:
		if sys.argv[1] == "--interactive":
			interactive = True
			print "Loading components..."
			
	corenlp = StanfordCoreNLP(corenlp_dir)
	output = open("dependency-density", "w")
	while True:
		if interactive:
			sentence = raw_input("Please enter a sentence: ")
		else:
			sentence = raw_input()
	
		if sentence == "":
			break
	
		parseoutput = eval(corenlp.parse(sentence))
		parsetree = parseoutput["sentences"][0]["parsetree"]
		
		terminals = len(parseoutput["sentences"][0]["words"])
		non_terminals = 0
		for c in parsetree:
			if (c == '('): non_terminals += 1
		non_terminals -= terminals
	
		dd = float(non_terminals) / terminals
	
		if interactive:
			print "The dependency density of the sentence is: ", dd
		else:
			output.write(str(dd) + '\n')
	if not(interactive):
		output.close()
if __name__ == '__main__':
	main()
