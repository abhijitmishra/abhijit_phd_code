'''
Created on 19-Nov-2013
Computes all the features and returns a dictionary
@author: abhijit
'''
from corenlp import StanfordCoreNLP
from corenlpConfig import corenlp_dir

########## Importing Features ##############################
from coreference_distance import compute_coref_distance
from degree_of_polysemy import dp
from structural_complexity import compute_sc
from word_length import compute_syllable_count
from discourse_connectors import count_discourse_connectors
from terminal_nonterminal import terminal_non_terminal_ratio
from percentage_noun_verb_prep import count_prepositions_noun_verb
from out_vocabulary import out_vocabulary
from numbers import num_count
from named_entity import named_entities
from length import length
from height_hypernym import height_hypernym_sentence
from passive import compute_passive
import sys
print "Loading Stanford CoreNLP module..."
corenlp = StanfordCoreNLP(corenlp_dir)
print "Stanford CoreNLP module loaded..."
#Sentence~L~Tp~DP~SC~TermNonterm~CorrDist~DisCon~HeighHyp~NE~NumCount~OOV~Passive~Pnoun~Pverb ~Pprep~SyllCount
def all_features(text,tgt_language):
    parseoutput = eval(corenlp.parse(text))
    feat_values = {}
    print >> sys.stderr ,  "Computing Length..."
    feat_values[u'L'] = length(text)
    print >> sys.stderr ,  "Computing Degree of Polysemy..."
    #print >> sys.stderr ,  "flag"
    feat_values[u'DP'] = dp(text)
    #print >> sys.stderr ,  "flag"
    print >> sys.stderr ,  "Computing Structural Complexity (dependency distance)..."
    feat_values[u'SC'] =compute_sc(text.replace("-","@"),parseoutput) #This replacement is to avoid confusion
    #print >> sys.stderr ,  "flag"
    print >> sys.stderr ,  "Computing Structural Complexity (Non terminal to terminal ratio)..."
    feat_values[u'Ddensity'] = terminal_non_terminal_ratio(text,parseoutput)
    #print >> sys.stderr ,  "flag"
    print >> sys.stderr ,  "Computing corefference distance.."
    feat_values[u'CorrDist'] = compute_coref_distance(text.decode("utf-8"),parseoutput)
    #print >> sys.stderr ,  "flag"
    print >> sys.stderr ,  "Computing discourse connector count..."
    feat_values[u'DisCon'] = count_discourse_connectors(text)
    #print >> sys.stderr ,  "flag"
    print >> sys.stderr ,  "Computing height of hypernymy..."
    feat_values[u'HeighHyp'] = height_hypernym_sentence(text)
    #print >> sys.stderr ,  "flag"
    print >> sys.stderr ,  "Computing Named Entity Count..."
    feat_values[u'NE'] = named_entities(text)
    #print >> sys.stderr ,  "flag"
    print >> sys.stderr ,  "Computing digit count..."
    feat_values[u'NumCount'] = num_count(text)
    #print >> sys.stderr ,  "flag"
    print >> sys.stderr ,  "Computing average OOV words based on GSL and AWL lists..."
    feat_values[u'OOV'] = out_vocabulary(text)
    #print >> sys.stderr ,  "flag"
    print >> sys.stderr ,  "Computing number of passive clauses ..."
    feat_values[u'Passive'] = compute_passive(text,parseoutput)
    #print >> sys.stderr ,  "flag"   
    print >> sys.stderr ,  "Computing average number of syllables... "
    feat_values[u'SyllCount'] = compute_syllable_count(text)
    #print >> sys.stderr ,  "flag"
    print >> sys.stderr ,  "Computing percentage of noun verb and prepositions..."
    feat_values[u'Pprep'],feat_values[u'Pnoun'],feat_values[u'Pverb'] = count_prepositions_noun_verb(text,parseoutput)
    return feat_values

if __name__ == '__main__':
    if (len(sys.argv)==1):
        print "Please input a sentence"
        sentence = raw_input()
        print "Please specify a target language"
        language = raw_input()
        print all_features(sentence,language)
        exit(0)
