# This program gives the structural complexity of a sentence
#Author: Nivvedan
import sys



def compute_sc(sentence,parseoutput):
	
	words = sentence.split()
	dependencies = parseoutput["sentences"][0]["indexeddependencies"]

	sc = 0;

	for dependency in dependencies:
		if dependency[0] != 'root':
			#print dependency[1], dependency[2]
			sc += abs(int(dependency[1].split("-")[1]) - int(dependency[2].split("-")[1]))

	sc = float(sc) / len(words)
	return sc

def main():
	from corenlpConfig import corenlp_dir
	from corenlp import StanfordCoreNLP
	interactive = False
	
	if len(sys.argv) > 1:
		if sys.argv[1] == "--interactive":
			interactive = True
			print "Loading components..."
	
	corenlp = StanfordCoreNLP(corenlp_dir)
	print "Components loaded..."
	output = open("structural-complexity", "w")
	
	while True:
		if interactive:
			sentence = raw_input("Please enter a sentence: ")
		else:
			sentence = raw_input()
	
		if sentence == "":
			break
		sentence = sentence.replace("-","@") #replacing hyphen to avoid confusion
		parsetree = eval(corenlp.parse(sentence));
		sc = compute_sc(sentence,parsetree)
		if interactive:
			print "The structural complexity of the sentence is: ", sc
		else:
			output.write(str(sc) + '\n')
	
	output.close()
if __name__ == '__main__':
  main()
