#compute product and save ScaComp_H

import csv
import sys

inf = csv.reader(open(sys.argv[1])) # participant_id, doc_id, attribute1, attribute2.... make sure attribs are normalized
outdir = sys.argv[2] # provide output dir here
out = open(outdir+"/scacomp_h.csv","w")
out.write("Participant, DocID, Baseline, ScaComp_H\n") 
header = 1
for row in inf:
	if header==1:
		header=0
		continue
	vals = map(float,row[2:])
	vals = filter(lambda z:z!=0,vals) #remove zero
	prod = reduce(lambda x,z:x*z,vals)
	out.write(row[0]+","+row[1]+ ","+str(vals[0])+","+str(prod) +"\n")
out.close()
	
	

