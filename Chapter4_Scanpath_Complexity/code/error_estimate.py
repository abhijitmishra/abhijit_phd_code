#compute MAE, MPE and Correlation with gold


from numpy import array
from scipy.stats.stats import pearsonr 
import sys, csv, math
def correlate(x,y):
	x = array(x)
	y = array(y)
	return pearsonr(x,y)[0]

def mae(x,y):
	comb = [x,y]
	comb = zip(*comb)
	m = sum(map(lambda x:abs(x[0]-x[1]),comb))/float(len(x))
	return m
def rmse(x,y):
	comb = [x,y]
	comb = zip(*comb)
	m = math.sqrt(sum(map(lambda x:(x[0]-x[1])*(x[0]-x[1]),comb))/float(len(x)))
	return m
gold_file = csv.reader(open(sys.argv[1]))
predicted_file = csv.reader(open(sys.argv[2]))

header = 0
x = []
y = []
for row in gold_file:
	if header==0:
		header=1
		continue
	x.append(float(row[2]))
	
header = 0
y = []
for row in predicted_file:
	if header==0:
		header=1
		continue
	y.append(float(row[2]))
		
print "MAE: ",mae(x,y)
print "rMSE: ",rmse(x,y)
print "Correlation: ",correlate(x,y)
