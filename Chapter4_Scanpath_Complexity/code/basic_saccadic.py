#Computes basic saccadic attributes

def compute_regression_count(fix_seq,normalize=False):
	'''
	fix_seq: sequence of word ids on the text which are fixated
	normalize: whether to normalize by number of fixations
	'''
	reg = 0
	for i in range(len(fix_seq)-1):
		if int(fix_seq[i])>int(fix_seq[i+1]):
			reg+=1
	if normalize ==True:
		reg/=float(fix_seq)
	return reg
def compute_saccadic_distance(fix_seq,normalize=False):
	'''
	fix_seq: sequence of word ids on the text which are fixated
	normalize: whether to normalize by number of fixations
	'''
	dist = 0
	prev = 0
	for f in fix_seq:
		dist += abs(f-prev)
	if normalize ==True:
		dist/=float(fix_seq)
	return dist
def compute_regression_distance(fix_seq,normalize=False):
	'''
	fix_seq: sequence of word ids on the text which are fixated
	normalize: whether to normalize by number of fixations
	'''
	reg_dist = 0
	for i in range(len(fix_seq)-1):
		if int(fix_seq[i])>int(fix_seq[i+1]):
			reg_dist+=abs(fix_seq[i]-fix_seq[i+1])
	if normalize ==True:
		reg_dist/=float(fix_seq)
	return reg_dist
