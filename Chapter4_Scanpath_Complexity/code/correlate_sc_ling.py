import csv,sys
from numpy import array,std,average
from scipy.stats import pearsonr
from scipy.stats import spearmanr
from sklearn.preprocessing import normalize
	

def correlate(x,y):
	'''
	x,y: Two arrays of same length whose correlation has to be computed
	'''
	x = array(map(float,x))
	y = array(map(float,y))
	rho,p= spearmanr(x,y)
	return [rho,p]

in_file = csv.reader(open(sys.argv[1])) #format: participant_id, docid, value1,value2...
ling_file = csv.reader(open(sys.argv[2])) #format: docid, value1,value2...
out_dir = sys.argv[3] #Correlations to be dumped here

#########Load the file with info regarding linguistic information######
header = 0
ling_headers = []
ling_data = []
for row in ling_file:
	if header==0:
		header=1
		ling_headers = row[1:]
		continue
	doc_id = row[0]
	rest = map(float,row[1:])
	ling_data.append(rest)

#########Load the file with scanpath complexity information######
header = 0
scanpath_complexity_headers = []
scanpath_complexity_data = {}
for row in in_file:
	if header==0:
		header=1
		scanpath_complexity_headers = row[2:]
		continue
	participant = row[0]
	doc_id = row[1]
	rest = map(float,row[2:])
	participant_data = scanpath_complexity_data.get(participant,[])
	participant_data.append(rest)
	scanpath_complexity_data[participant] = participant_data
	
participants = scanpath_complexity_data.keys()
participants.sort()
overall = {}
ks = []
file_key = sys.argv[1].replace(".csv","").split("/")[-1]
output = open(out_dir+"/"+"Participant_wise_correlation_"+file_key+".csv","w")
output.write("Participant,Combination,Correlation\n")
for p in participants:
	scanpath_complexity = scanpath_complexity_data[p]
	scanpath_complexity = zip(*scanpath_complexity)
	ling = zip(*ling_data)  # Transpose the linguistic complexity data so that each row corresponds to all documents and one value of linguistic complexity
	for i in range(len(scanpath_complexity)):
		for j in range(len(ling)):
			sc_header = scanpath_complexity_headers[i]
			ling_header = ling_headers[j]
			s = scanpath_complexity[i]
			l = ling[j] 
			rho,pr = correlate(s,l)
			output.write(",".join([p,sc_header+"-"+ling_header,str(rho)])+"\n")
			od = overall.get(sc_header+"-"+ling_header,[]) 
			od.append(rho)
			overall[sc_header+"-"+ling_header] = od
			if sc_header+"-"+ling_header not in ks:
				ks.append(sc_header+"-"+ling_header)
output.close()	

output1 = open(out_dir+"/"+"Overall_correlation_"+file_key+".csv","w")
output1.write("Combination,Mean,Stdev\n")

for k in ks:
	val = overall[k]
	avg = average(val)
	st = std(val)
	output1.write(",".join([k,str(avg),str(st)])+"\n")
	
output1.close()
