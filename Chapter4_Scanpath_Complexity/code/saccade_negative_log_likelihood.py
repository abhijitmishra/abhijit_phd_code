#initialization for the skew gaussian
import math
def find_char_distance(wid1,wid2, word_length):
	if wid1<wid2:
		return sum(word_length[wid1:wid2])
	else: 
		return -1*sum(word_length[wid2:wid1])
def compute_assymmetric_gaussian_progression(x):
	sigma_l = 22
	sigma_r = 18 #
	psi = 0.92
	rho = 13 #boundary to the right is 13 characters
	x=x-rho
	Z = float(math.sqrt(math.pi/2)*(sigma_l+sigma_r))
	if x>0 or x==0:
		lambda_x =(1/Z)*math.exp(-(x*x)/(2*sigma_r*sigma_r))
	else:
		lambda_x =(1/Z)*math.exp(-(x*x)/(2*sigma_l*sigma_l))
	return psi*lambda_x
def compute_assymmetric_gaussian_regression(x):
	sigma_l = 13
	sigma_r = 3
	rho = 8 #boundary to the left is 7 characters
	psi = 0.08
	x=x-rho
	Z = float(math.sqrt(math.pi/2)*(sigma_l+sigma_r))
	if x>0 or x==0:
		lambda_x =(1/Z)*math.exp(-(x*x)/(2*sigma_r*sigma_r))
	else:
		lambda_x =(1/Z)*math.exp(-(x*x)/(2*sigma_l*sigma_l))
	return psi*lambda_x
def compute_saccadic_complexity_negative_log_likelihood(fix_seq,word_length,normalize=False):
	'''
	fix_seq: sequence of word ids on the text which are fixated
	word_length: A list containing length of the words in the order they appear in the text
	normalize: whether to normalize by number of fixations
	'''
	nll = 0.0
	current_word = 0
	prev_fix = -1 #to capture regression info the value is initialized to -1
	try:
		for s in fix_seq:
			dist = find_char_distance(current_word,s,word_length)
			if s<prev_fix:
				#regression
				p = compute_assymmetric_gaussian_regression(dist)
			else:
				p = compute_assymmetric_gaussian_progression(dist)
			prev_fix = s
			try:
				ll = math.log(p)
			except:
				#print dist
				ll = -200 # a high value for p being close to 0
			current_word = s
			nll+=ll
		nll*=-1 #since complexity is negative of log likelihood
		if normalize ==True:
			sacc_count = len(fix_seq)-1
			nll = nll/float(sacc_count)
	except:
		print "Problem in NLL computation"
	return nll
