def process_IA(ia_path,ids):
	ia_dat = {}
	f = open(ia_path,"r")
	reader = csv.reader(f)
	header = 0
	for row in reader:
		if header ==0:
			header = 1
			continue
		doc_id = row[0]
		if doc_id in ids:
			ia_id = row[1]
			ia_len = len(row[2])
			doc_data = ia_dat.get(doc_id,[])
			doc_data.append(ia_len)
			ia_dat[doc_id] = doc_data
	f.close()
	return ia_dat

def process_fix_data(fix_data_path,ia_dat,ids):
	data = {}
	f =open(fix_data_path,"r")
	reader = csv.reader(f)
	header = 0
	for row in reader:
		if header==0:
			header=1
			continue
		participant = row[0]
		document = row[1]
		if document in ids:
			word_id = int(row[2])
			duration =  int(row[3])
			participant_data = data.get(participant,{})
			document_data = participant_data.get(document,[])
			document_data.append([word_id,duration])
			participant_data[document] = document_data
			data[participant] = participant_data
	new_data = {}
	for key in data.keys():
		new_document_data = {}
		participant_data = data[key]
		docs = participant_data.keys()
		for doc in docs:
			document_data = participant_data[doc]
			fix_ids, fix_durs = zip(*document_data) #get Fix_id, Fix d
			word_length_info = ia_dat.get(doc,[])
			if word_length_info==[]:
				print "Invalid data for doc "+doc
			else:
				new_document_data[doc] = [fix_ids,fix_durs,word_length_info]
		new_data[key] = new_document_data
	return new_data

if __name__=="__main__":
	import sys
	import csv
	from basic_fixation import compute_total_fixation_duration#(fix_seq,fix_dur_seq,normalize=False)
	from basic_fixation import compute_first_fixation_duration#(fix_seq,fix_dur_seq,normalize=False)
	from basic_fixation import compute_regression_fixation_duration#(fix_seq,fix_dur_seq,normalize=False)
	from basic_fixation import compute_skip_percentage#(fix_seq,fix_dur_seq,word_count,normalize=False)
	from basic_fixation import compute_fixation_count#(fix_seq,fix_dur_seq)
	from basic_saccadic import compute_regression_count#(fix_seq,normalize=False)
	from basic_saccadic import compute_saccadic_distance#(fix_seq,normalize=False)
	from basic_saccadic import compute_regression_distance#(fix_seq,normalize=False)
	from saccade_graph_entropy import compute_saccadic_complexity_graph #(fix_seq,word_length,normalize=False) 
	from saccade_negative_log_likelihood import compute_saccadic_complexity_negative_log_likelihood#(fix_seq,word_length,normalize=False)
	
	fix_file = sys.argv[1]
	ia_file = sys.argv[2]
	ids = map(str,range(1,1001)) # If we want to filter out ids we should specify here
	ia_dat = process_IA(ia_file,ids)
	fix_dat = process_fix_data(fix_file,ia_dat,ids)
	output = open(fix_file.replace(".csv","_scanpath_attributes.csv"),"w") #creates a new file in which all scanpath attributes are saved
	output.write("Participant, Doc_id,FD,FFD, RFD,FC,SKIP, RC,SD,RD,NLL\n")
	participants = fix_dat.keys()
	participants.sort()
	for p in participants:
		document = fix_dat[p]
		doc_ids = document.keys()
		doc_ids.sort(key=int)
		for d in doc_ids:
			fix_seq, fix_dur_seq,word_len = document[d]
			fix_seq = list(fix_seq)
			fix_dur_seq = list(fix_dur_seq)
			word_count = len(word_len)
			
			FD = compute_total_fixation_duration(fix_seq,fix_dur_seq)
			FFD = compute_first_fixation_duration(fix_seq,fix_dur_seq)
			RFD = compute_regression_fixation_duration(fix_seq,fix_dur_seq)
			FC = compute_fixation_count(fix_seq,fix_dur_seq)
			SKIP = compute_skip_percentage(fix_seq,fix_dur_seq,word_count)
			RC = compute_regression_count(fix_seq)
			SD = compute_saccadic_distance(fix_seq)
			RD = compute_regression_distance(fix_seq)
			NLL = compute_saccadic_complexity_negative_log_likelihood (fix_seq,word_len)
			#We switch off graph entropy for the moment since it does not seem to add any significant value
			#GE = compute_saccadic_complexity_graph (fix_seq,word_len)
		
			out = [p,d, FD, FFD, RFD, FC, SKIP, RC, SD, RD, NLL]
			out = map(str, out)
			output.write(",".join(out)+"\n")
output.close()
