import math
def degree(adj):
	degree = []
	for r in adj:
		degree.append(sum(r))
	return degree
def get_adjacency_matrix(fix_seq,word_length):
	'''
	fix_seq: sequence of word ids on the text which are fixated
	word_length: A list containing length of the words in the order they appear in the text
	'''
	word_ids = range(len(word_length))
	adj = [[0 for i in word_ids] for j in word_ids]
	for i in range(len(fix_seq)-1):
		curr_id = fix_seq[i]-1 #compensate for 0 subscript
		nxt_id = fix_seq[i+1]-1 #compensate for 0 subscript
		adj[curr_id][nxt_id] = 1
		adj[nxt_id][curr_id] = 1
	return adj

def graph_entropy_sole_valverde(adj):
	'''
	adj: adjacency matrix of the graph
	'''
	# Calculate Sole & Valverde, 2004 graph entropy
	# Uses Equations 1 and 4
	# First we need the denominator of q(k)
	# To get it we need the probability of each degree
	# First get the number of nodes with each degree
	existing_degrees  = degree(adj)
	max_degree = len(adj)
	all_degrees = range(max_degree)
	degree_dist = [[0]*(len(all_degrees)+1) for i in range(3)]#Need an extra zero prob degree for later calculations
	degree_dist[0] = range(max_degree+1)
	for a_degree in all_degrees:
		degree_dist[1][a_degree] = existing_degrees.count(a_degree)
	
	#Calculate probability of each degree
	for a_degree in all_degrees:
		degree_dist[2][a_degree] = float(degree_dist[1][a_degree])/float(sum(degree_dist[1]))
	#Sum of all degrees multiplied by their prob
	sum_pk = 0
	for a_degree in all_degrees:
		sum_pk+=a_degree*degree_dist[2][a_degree]#degree_dist[1][a_degree]*degree_dist[2][a_degree]
	graph_entropy = 0
	a =[]
	for a_degree in range(max_degree):
		q_of_k = float((a_degree)*degree_dist[2][a_degree])/float(sum_pk)
		a.append(q_of_k)
		#print q_of_k
		if q_of_k!=0:
			graph_entropy+=(-1*q_of_k*math.log(q_of_k,2))
	#print "sum: "+str(sum(a))
	return graph_entropy

def compute_saccadic_complexity_graph(fix_seq,word_length,normalize=False):
	'''
	fix_seq: sequence of word ids on the text which are fixated
	word_length: A list containing length of the words in the order they appear in the text
	normalize: whether to normalize by number of fixations
	'''
	try:
		adj = get_adjacency_matrix(fix_seq,word_length)
		ge_sol = graph_entropy_sole_valverde(adj)
		if normalize==True:
			ge_sol = float(ge_sol)/len(fix_seq)
		return ge_sol
	except:
		print "Graph entropy could not be computed. Div by zero"
		return 0.0
