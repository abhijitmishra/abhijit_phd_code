def compute_total_fixation_duration(fix_seq,fix_dur_seq,normalize=False):
	'''
	fix_seq: sequence of word ids on the text which are fixated
	fix_dur_seq: list of duration of fixations in the fixation sequence 
	normalize: whether to normalize by number of fixations 
	'''
	fd = sum(fix_dur_seq)
	if normalize==True:
		#Normalize complexity based on word count
		fd = fd/float(len(fix_seq))
	return fd

def compute_first_fixation_duration(fix_seq,fix_dur_seq,normalize=False):
	'''
	fix_seq: sequence of word ids on the text which are fixated
	fix_dur_seq: list of duration of fixations in the fixation sequence
	normalize: whether to normalize by number of fixations 
	'''
	fixdata = {}
	for i in range(len(fix_seq)):
		fix = fix_seq[i]
		if fix not in fixdata.keys():
			fixdata[fix] = fix_dur_seq[i]
	ffd = sum(fixdata.values())
	if normalize==True:
		#Normalize complexity based on word count
		ffd = ffd/float(len(fix_seq))
	return ffd
	
def compute_regression_fixation_duration(fix_seq,fix_dur_seq,normalize=False):
	'''
	fix_seq: sequence of word ids on the text which are fixated
	fix_dur_seq: list of duration of fixations in the fixation sequence
	normalize: whether to normalize by number of fixations 
	'''
	reg_dur_data = []
	prev_fix = -1
	for i in range(len(fix_seq)):
		fix = fix_seq[i]
		if prev_fix > fix:
			#regression
			reg_dur_data.append(fix_dur_seq[i])
		prev_fix = fix
	rfd = sum(reg_dur_data)
	if normalize==True:
		rfd = rfd/float(fix_seq)
	return rfd
def compute_skip_percentage(fix_seq,fix_dur_seq,word_count,normalize=False):
	'''
	fix_seq: sequence of word ids on the text which are fixated
	fix_dur_seq: list of duration of fixations in the fixation sequence
	word_count: total number of words in the text
	normalize: whether to normalize by number of fixations 
	'''
	fixation_set = set(fix_seq)
	skip_count = word_count-len(fixation_set)
	if word_count<len(fixation_set):
		return 0
	skip_count /= float(word_count)
	if normalize==True:
		skip_count = skip_count/float(fix_seq)
	return skip_count
def compute_fixation_count(fix_seq,fix_dur_seq):
	'''
	fix_seq: sequence of word ids on the text which are fixated
	fix_dur_seq: list of duration of fixations in the fixation sequence
	'''
	return len(fix_seq)
