from sklearn import linear_model
from sklearn.feature_selection import f_regression
from sklearn.linear_model import LinearRegression
from sklearn.metrics import precision_recall_fscore_support,roc_auc_score,confusion_matrix
from sklearn.cross_validation import KFold
from numpy import array
import numpy as np
import scipy as sc

def process_arff(f):
	features = []
	labels = []
	for row in f:
		row = row.strip()
		if row=="":
			continue
		if row[0]=="@":
			continue
		else:
			row = row.replace("'","")
			inf = row.split(",")
			featureVec = inf[:-1]
			featureVec = map(float,featureVec)
			label = int(inf[-1])
			features.append(featureVec)
			labels.append(label)
	return [features,labels]

def evaluate(prediction, actual):
	precision, recall, f_beta,support = precision_recall_fscore_support(actual,prediction,average="weighted")
	precision = array([precision,0])
	recall = array([recall,0])
	f_beta = array([f_beta,0])
	return [precision, recall, f_beta]
def train(features,labels):
	clf = LinearRegression(normalize =True)
	model = clf.fit(features,labels)
	return model
def test(model,test_features,test_labels):
	prediction = model.predict(test_features)
	precision, recall, f_beta= evaluate(prediction, test_labels)
	return [precision,recall,f_beta]
def k_fold_validation(features,labels,k):
	kf = KFold(len(labels), n_folds=k)
	features = array(features)
	labels = array(labels)
	average_precision = array([0.0,0.0])
	average_recall = array([0.0,0.0])
	average_f = array([0.0,0.0])
	for train_index,test_index in kf:
		X_train,X_test = features[train_index],features[test_index]
		Y_train,Y_test = labels[train_index],labels[test_index]
		model = train(X_train,Y_train)
		precision,recall,f_beta,kappa = test(model,X_test,Y_test)
		average_precision+=precision
		average_recall+=recall
		average_f +=f_beta
		
		print "........................."
		print "Precision: " +str(list(precision))
		print "Recall: "+str(list(recall))
		print "F: "+str(list(f_beta))
		print "........................."
	print "Average statistics"
	print "Avg Precision: " +str(list(average_precision/float(k)))
	print "Avg Recall: "+str(list(average_recall/float(k)))
	print "Avg F: "+str(list(average_f/float(k)))

if __name__=="__main__":
	import sys,csv
	from scipy import stats
	#Input file: participant, doc_id, attribute1,....attributeN, annotated complexity
	inf = open(sys.argv[1],"r")
	reader = csv.reader(inf)
	header = 0
	participant_feature_data = {}
	participant_label_data = {}
	total_train_features = []
	total_train_labels = []
	baseline_train_feature = []
	for row in reader:
		if header ==0:
			header=1
			continue
		participant = row[0]
		document = row[1]
		features = map(float,row[2:-1])
		baseline_feature = float(row[2]) #Total annotation time
		label = float(row[-1])
		pfd = participant_feature_data.get(participant,[])
		pld = participant_label_data.get(participant,[])
		pfd.append(array(features))
		total_train_features.append(array(features))
		total_train_labels.append(array(label))
		pld.append(label)
		participant_feature_data[participant]= pfd
		participant_label_data[participant]= pld
		baseline_train_feature.append(array([baseline_feature]))
	participants = participant_feature_data.keys()
	participants.sort()
	for p in participants:
		#Print participant wise ANOVA significance: Not reported in the paper
		print p
		pfd = participant_feature_data[p]
		pld = participant_label_data[p]
		model = f_regression(array(pfd),array(pld))
		print model
	print "Total Significance"
	model1 = f_regression(array(total_train_features),array(total_train_labels))
	print model1
	
	clf = LinearRegression(normalize =True)
	model1 = clf.fit(array(total_train_features),array(total_train_labels))
	scores = model1.predict(array(total_train_features))
	print "scores"
	for s in scores:
		#Prints predicted values 
		print s
