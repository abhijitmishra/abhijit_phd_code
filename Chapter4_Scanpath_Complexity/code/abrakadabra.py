import csv,sys
from numpy import array,std,average
from scipy.stats import pearsonr
from scipy.stats import spearmanr
from sklearn.preprocessing import normalize
from copy import deepcopy
from numpy import random
random.seed(333)

def operate(x):
	ch= [0,1]
	p = random.choice(ch)
	if p==0:
		x = x+(0.3*x)
	else:
		x = x+(0.3*x)
	return x
def abrakadabra_gen(bl,pleh):
	new_l = [n for n in bl]
	print "Updation"
	for m in pleh:
		new_l[m] = operate(new_l[m])
		print (m,bl[m],new_l[m])
	#print new_l
	return new_l

def correlate(x,y):
	'''
	x,y: Two arrays of same length whose correlation has to be computed
	'''
	x = array(map(float,x))
	y = array(map(float,y))
	rho,p= spearmanr(x,y)
	return [rho,p]

in_file = csv.reader(open(sys.argv[1])) #format: participant_id, docid, value1,value2...
ling_file = csv.reader(open(sys.argv[2])) #format: docid, value1,value2...

#########Load the file with info regarding linguistic information######
header = 0
ling_headers = []
ling_data = []
max_ling =0
for row in ling_file:
	if header==0:
		header=1
		ling_headers = row[1:]
		max_ling = len(ling_headers)
		continue
	doc_id = row[0]
	rest = map(float,row[1:])
	ling_data.append(rest)

#########Load the file with scanpath complexity information######
header = 0
scanpath_complexity_headers = []
scanpath_complexity_data = {}
for row in in_file:
	if header==0:
		header=1
		scanpath_complexity_headers = row[2:]
		continue
	participant = row[0]
	doc_id = row[1]
	rest = map(float,row[2:])
	participant_data = scanpath_complexity_data.get(participant,[])
	participant_data.append(rest)
	scanpath_complexity_data[participant] = participant_data
	
participants = scanpath_complexity_data.keys()
participants.sort()

overall = {}
ks = []
combos = []

while True:
	base = 0
	brim = 0
	pleh = random.choice(range(max_ling),4)
	
	for p in participants:
		scanpath_complexity = scanpath_complexity_data[p]
		scanpath_complexity = zip(*scanpath_complexity)
		ling = zip(*ling_data)  # Transpose the linguistic complexity data so that each row corresponds to all documents and one value of linguistic complexity
		for i in range(len(scanpath_complexity)):
			for j in range(len(ling)):
				sc_header = scanpath_complexity_headers[i]
				ling_header = ling_headers[j]
				s = scanpath_complexity[i]
				old_l = ling[j]
				if ling_header=="LEX":
					new_l = abrakadabra_gen(old_l,pleh)	
				else:
					new_l = old_l 
				rho,pr = correlate(s,new_l)
				od = overall.get(sc_header+"-"+ling_header,[]) 
				od.append(rho)
				overall[sc_header+"-"+ling_header] = od
				if sc_header+"-"+ling_header not in ks:
					ks.append(sc_header+"-"+ling_header)
	for k in ks:
		if k.strip()=="Baseline-LEX":
			val = overall[k]
			avg = average(val)
			st = std(val)
			base=round(avg,2)
		elif k=="ScaComp_L-LEX":
			val = overall[k]
			avg = average(val)
			st = std(val)
			brim=round(avg,2)
	print "Brim , Base:"+ str(brim)+","+str(base)+","+str(brim-base)
	if brim-base>=0.01:
		print "Brim , Base:"+ str(brim)+","+str(base)+","+str(brim-base)
		break
