Case 1 (Structural Complexity):

Simple:

The Unbearable Lightness of Being is a 1988 American erotic romantic drama movie. It is based on the novel of the same name. The movie is set in 1968 in Prague and Switzerland. Daniel Day-Lewis plays brain surgeon Tomas. Lena Olin plays Sabina. Juliette Binoche plays Tereza. This movie was released in February 1988.

Domain: Literature 

ID: 28 

Word Count: 52
Sentence Count: 7

Lexical Density: 0.61
OOV: 0.61
Structural Complexity: 45

Lexile Score: 600
Flesch Kincaid Readability : 6.8


Complex:

The Unbearable Lightness of Being is a 1988 American film adaptation of the novel of the same name by Milan Kundera, published in 1984. Director Philip Kaufman and screenwriter Jean-Claude Carrière portray the effect on Czechoslovak artistic and intellectual life during the 1968 Prague Spring of socialist liberalization preceding invasion by Soviet led Warsaw Pact and subsequent coup that ushered in hard-line communism.


Domain: Literature 

ID: 27

Word Count: 62
Sentence Count: 2

Lexical Density: 0.62
OOV: 0.57
Structural Complexity: 69

Lexile Score: 1700
Flesch Kincaid Readability : 18

Observations:


Case 2 (Lexical Density):

Simple:

British India was the area of India in South Asia which for hundreds of years was under the influence of the English (later the British). From the 1600s until 1858 these areas were run by the English East India Company. After 1858 until 1947 they became the British Raj. Some areas were under the direct rule of the Governor-General of India. He was appointed by the Government of the United Kingdom in London, and was a Viceroy, meaning, the deputy of Queen Victoria. In princely states where an agreement was reached, the traditional rule continued, but the British had an influence. After 1876 when Queen Victoria become Empress of India, British India was part of the British Indian Empire, which also included hundreds of Indian princely states which had never been conquered by the British and still had control of their own affairs. These were each ruled by local rulers under the protection of the British. This empire is sometimes called the British Raj.

Domain: History

ID: 8

Word Count: 161 
Sentence Count: 9 

Lexical Density: 0.48
OOV: 0.43
Structural Complexity: 47

Lexile Score: 1120
Flesch Kincaid Readability: 10.1  



Complex:

Provinces of India, earlier Presidencies of British India and still earlier, Presidency towns, were the administrative divisions of British governance in the subcontinent. In one form or other they existed between 1612 and 1947, conventionally divided into three historical periods. During 1612–1757, the East India Company set up "factories" (trading posts) in several locations, mostly in coastal India, with the consent of the Mughal emperors or local rulers. Its rivals were the merchant trading companies of Holland and France. By the mid-18th century, three "Presidency towns": Madras, Bombay, and Calcutta had grown in size. During the period of Company rule in India, 1757–1858, the Company gradually acquired sovereignty over large parts of India, now called "Presidencies." However, it also increasingly came under British government oversight, in effect sharing sovereignty with the Crown. At the same time it gradually lost its mercantile privileges. Following the Indian Rebellion of 1857, the Company's remaining powers were transferred to the Crown. In the new British Raj (1858–1947), sovereignty extended to a few new regions, such as Upper Burma. Increasingly, however, unwieldy presidencies were broken up into "Provinces".


Domain: History

ID: 7

Word Count: 178
Sentence Count: 11 

Lexical Density: 0.56
OOV: 0.46
Structural Complexity: 56 

Lexile Score: 1150
Flesch Kincaid Readability : 11.8 


Case 3 (Readability):

Simple:


The atomic bombings of Hiroshima and Nagasaki were nuclear attacks on the Empire of Japan during World War II (WWII). The United States and the Allies were fighting against Japan and slowly winning. Two nuclear bombs were dropped, one on the city of Hiroshima and one on the city of Nagasaki. U.S. President Harry S. Truman ordered these attacks on August 6 and 9, 1945. This was near the end of WWII. The atomic bombs had been created through the Manhattan Project. Nuclear bombs are much more powerful than other kinds of bombs. By the end of 1945, the bombs had killed as many as 140,000 people in Hiroshima and 80,000 in Nagasaki. (The generals wanted to bomb Kokura instead of Nagasaki, but it was too cloudy over Kokura that day). In both of the bombed cities, most of the people who died were civilians, people that are not soldiers.


Domain: History

ID: 2

Word Count: 145 
Sentence Count: 13  

Lexical Density: 0.49
OOV: 0.42
Structural Complexity: 37

Lexile Score: 970
Flesch Kincaid Readability: 5.9 


Complex:

The United States, with the consent of the United Kingdom as laid down in the Quebec Agreement, dropped nuclear weapons on the Japanese cities of Hiroshima and Nagasaki in August 1945, during the final stage of World War II. The two bombings, which killed at least 129,000 people, remain the only use of nuclear weapons for warfare in history. In the final year of the war, the Allies prepared for what was anticipated to be a very costly invasion of the Japanese mainland. This was preceded by a U.S. firebombing campaign that obliterated many Japanese cities. The war in Europe had concluded when Nazi Germany signed its instrument of surrender on May 8, 1945. The Japanese, facing the same fate, refused to accept the Allies' demands for unconditional surrender and the Pacific War continued. Together with the United Kingdom and China, the United States called for the unconditional surrender of the Japanese armed forces in the Potsdam Declaration on July 26, 1945—the alternative being "prompt and utter destruction". The Japanese response to this ultimatum was to ignore it.

Domain: History

ID: 1

Word Count: 174
Sentence Count: 10  

Lexical Density: 0.53
OOV: 0.43
Structural Complexity: 105

Lexile Score: 1290
Flesch Kincaid Readability : 10.8

Case 4: (Length)

Simple:

Dracula is a horror novel written by the Irish writer Bram Stoker. Bram Stoker published the novel in England in 1897. Sir Henry Irving was an actor and friend of Bram Stoker. The character 'Dracula' may have been based in part on Irving, and on Vlad III the Impaler. The story in the novel happens in England and Transylvania in 1893.

ID: 26

Word Count: 59 
Sentence Count: 5  

Lexical Density: 0.52
OOV: 0.50
Structural Complexity: 25 

Lexile Score: 860
Flesch Kincaid Readability : 7.6



Complex:

Dracula is an 1897 Gothic horror novel by Irish author Bram Stoker, famous for introducing the character of the vampire Count Dracula. The novel tells the story of Dracula's attempt to move from Transylvania to England so that he may find new blood and spread the undead curse, and of the battle between Dracula and a small group of men and women led by Professor Abraham Van Helsing. Dracula has been assigned to many literary genres including vampire literature, horror fiction, the gothic novel, and invasion literature. Stoker did not invent the vampire but he defined its modern form, and the novel has spawned numerous theatrical, film, and television interpretations.

ID: 25

Word Count: 109 
Sentence Count: 4   

Lexical Density: 0.61
OOV: 0.41
Structural Complexity: 55 

Lexile Score: 1580
Flesch Kincaid Readability : 14.7

